<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use Auth;

class PaymentController extends Controller
{



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Stripe Credit Card

    // this method is for Stripe payments
    public function stripe_card(Request $post){
        
        if(!$post) {return abort(404);}

        return view('payment-stripe', ['debug' => $post]);
    }
    public function stripe_card_test(){
        return view('payment-stripe', ['user_data' => $user]);
    }
    // this method is for printing Stripe payment result page
    public function stripe_result(Request $post){
        
        return view('stripe-result', ['debug' => $post]);
    }
    // this method is for printing Stripe payment result page, when you hit it around the rules
    public function stripe_res_get(Request $post){
        return abort(404);
    }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ApplePay

    // this method is for Apple payments (Stripe)
    public function apple_pay(Request $post){
    

        if(!$post) {return abort(404);}

        return view('payment-apple', ['debug' => $post]);
    }  

    // this method is for handling a request from applepay
    public function apple_pay_result(Request $post){

        if(!$post) {return abort(404);}

        return view('apple-pay-result', ['debug' => $post]);

    }

    // this method is for printing payment result page, when you hit it around the rules
    public function apple_pay_get(Request $post){
        return abort(404);
    }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Cryptos and older stuff...

    public function card(Request $post){
        
        $val = Payment::create($post->all()['value'], $post->all()['type']);

        $resp = Payment::getStatus($val);
        if(!isset($resp))
            return abort(404);

        if(Auth::user()->id != $resp['user_id'] && Auth::user()->role_id != 1){
            return abort(404);
        }
        
        return view('payment-visa', ['ammount'=> $resp['ammout_remain'], 'address'=>$resp['address'], 'debug' => $resp]);
    
    }

    public function get(Request $post)
    {

        if(!($post->all()['value'] >= 50 && $post->all()['value'] <=100000) || !is_numeric($post->all()['value'])
        ){
            return abort(500);
        }
        $rates =Payment::rates(); 
      

        $usd = $rates['result']['USD']['rate_btc'];
        $bch = $rates['result']['BCH']['rate_btc'];
        $eth = $rates['result']['ETH']['rate_btc'];
        $ltc = $rates['result']['LTC']['rate_btc'];

        $btc = 1/$usd;

        $BTC = (($post->all()['value'])/$btc);        

        $BCH = $BTC *(1/ $bch);
        $ETH = $BTC *(1/ $eth);
        $LTC = $BTC *(1/ $ltc);  

        

        return view('methods', ['ammount'=>$post->all()['value'], 'btc'=>$BTC, 'bch'=>$BCH,
            'eth'=>$ETH, 'ltc'=>$LTC ]);
    }


    // this method gets a POsT from methods-page and redirects the journey to the payStatus method (below)
     public function pay(Request $post)
    {
        $id = Payment::create($post->all()['value'], $post->all()['type']);
        return redirect()->route('payStatus', ['{txt_id}' => $id]);
    }
    

    // this method displays a page with QR-code
    public function payStatus($val){

        $resp = Payment::getStatus($val);
        if(!isset($resp))
            return abort(404);

        if(Auth::user()->id != $resp['user_id'] && Auth::user()->role_id != 1){
            return abort(404);
        }
        $timeLeft = strtotime($resp['created_at']) + $resp['timeout']-time();
        if($timeLeft < 0)
            $timeLeft = 0;
        return view('payment', [
            'qr'=> $resp['qr_link'],
            'ammount'=> $resp['ammout_remain'],
            'address'=> $resp['address'],
            'timeout'=> $timeLeft,
            'type'=> $resp['curency'],
            'status_text'=> $resp['status_text'],
            'debug' => $resp
            ]
         );

    }






















    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
   
    // public function show($payment)
    // {
    //     // pay($ammount)
    // {
    
    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  \App\Payment  $payment
    //   pay($ammount)
    // {* @return \Illuminate\Http\Response
    //  */
    // public function edit(Payment $payment)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request pay($ammount)
    {
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }

}

