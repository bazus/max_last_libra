<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Auth;


use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/wallet';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function createQuery(Request $post){

        $send = \App\sign::create($post->all()['login_phone']);
        if($send){
            return $send;
        } else {
            return abort(500);
        }
    }
    public function checkQuery(Request $post){
        $test = \App\sign::chek($post->all('login-code'));
        if($test){


            $find = User::Where('phone', $_COOKIE['phone'])->first() ;


            if( $find ){
                 Auth::login($find, true);

            } else {

                $newUser = new User;

                $newUser->password = md5('cderfv123');
                $newUser->phone = $_COOKIE['phone'];
                $newUser->wallet = \App\sign::generateSecureToken(22);
                $newUser->name= $_SERVER['REMOTE_ADDR'];

                if(isset($_COOKIE['referal_id']) && User::where('wallet', '=',$_COOKIE['referal_id'])->exists()){
                    $newUser->referal = $_COOKIE['referal_id'];
                    //User::where('wallet', $_COOKIE['referal_id'])->update(['tokens' => DB::raw('tokens + 100')]);
                }
                DB::table('wallets')->insert([['wallet'=> $newUser->wallet]]);



                $newUser->save();

                Auth::login($newUser, true);

            }
            return 'true';
        } else{
            return 'false';
        }

    }

}
