<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use App\Payment;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function gets(Request $post){
        \App\gets::create($post->all()['wallet']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     return redirect(route('index'));
    // }
    public function wallet()
    {
        $time = time() - strtotime(Auth::user()->created_at);
        
        
        $user = Auth::user();
        return view('cabinet', ['wallet'=>$user->wallet,
            'coins'=>$user->tokens,
            'time' => $time
    ]);
    }
    public function invest()
    {
        return view('invest');
    }
    public function history(){
        //$transactions = \App\Payment::select('*')->where(['user_id' => Auth::user()->id])->get()->values();
        $transactions = DB::table('payments')->where([['user_id',Auth::user()->id]])->get()->reverse()->values();
        return view('history', ['transactions'=> $transactions]);
    }
    public function referal(){
        
        $ammount = DB::table('wallets')->where('wallet', Auth::user()->wallet)->first();

        $ammount = $ammount->balance;
        
        $rates =Payment::rates(); 
      

        $usd = $rates['result']['USD']['rate_btc'];
    

        $btc = 1/$usd;

        $BTC = (($ammount)/$btc);        

        $count = DB::table('users')->where('referal', Auth::user()->wallet)->count();


        return view('invite', ['ammount' => $ammount, 'btc'=>$BTC, 'count'=>$count]);

    }
}
