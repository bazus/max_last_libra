<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Default routes
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();
//////////////////////////////////////////////////////////////////////////////////////////

// Route::get('api', function(){
// 	$text = '{"data":{"status":"sent","code":1,"sms_id":"truwsp3rzkvf","price":1.19,"credit":247.62,"number":"436505661657"}}';
// 	echo "<pre>";
// 	print_r(json_decode($text)->data);
// 	echo "</pre>";

// });
//////////////////////////////////////////////////////////////////////////////////////////


//APi
Route::post('phone/callback','Auth\LoginController@createQuery');
Route::post('phone/code',	 'Auth\LoginController@checkQuery');



//Public pages
Route::get('/media', 					'PublicController@media')			->name('media');			//Media page
Route::get('/paper', 					'PublicController@paper')			->name('paper');			//Info
Route::get('/learn', 					'PublicController@learn')			->name('learn');			//Learn page
Route::get('/', 						'PublicController@index')			->name('index');			//Learn page
Route::get('/referal/{referal}',		'PublicController@referalRegister')	->name('referalRegister');	//Referal page
Route::get('/privacy',					'PublicController@privacy') 		->name('privacy');


//////////////////////////////////////////////////////////////////////////////////////////
//Route::get('/stripe-test', 				'PaymentController@stripe_card_test')->name('stripe_card_test');	//Info
//////////////////////////////////////////////////////////////////////////////////////////

//For logined user
Route::get('/history', 					'HomeController@history')				->name('history');
Route::get('wallet', 					'HomeController@wallet')				->name('wallet');			//Index, wallet, balace
Route::get('/invest',					'HomeController@invest')				->name('invest');			//First stage paying
Route::get('/referal',					'HomeController@referal')				->name('referal');			//Referal page


Route::post('/pay/card/',				'PaymentController@card')				->name('card');				//IPN callback
Route::post('/pay/callback',			'PublicController@callback')			->name('callback');			//IPN 

//Payments
Route::post('/invest',					'PaymentController@get')				->name('get');				//Second stage paying
Route::post('/pay', 					'PaymentController@pay')				->name('pay');				//Create payment

Route::post('/pay/card/stripe', 		'PaymentController@stripe_card')		->name('stripe_card');		//Create Stripe payment
Route::post('/pay/card/stripe/result', 	'PaymentController@stripe_result')		->name('stripe_result');	//Stripe payment result page
 Route::get('/pay/card/stripe/result', 	'PaymentController@stripe_res_get')		->name('stripe_res_get');	//Stripe payment result page trap

Route::post('/pay/apple-pay', 			'PaymentController@apple_pay')			->name('apple_pay');		//Create apple payment
Route::post('/pay/apple-pay/result', 	'PaymentController@apple_pay_result')	->name('apple_pay_result');	//Apple payment result page
 Route::get('/pay/apple-pay/result', 	'PaymentController@apple_pay_get')		->name('apple_pay_get');	//Apple payment result page trap

 Route::get('/pay/{txt_id}', 			'PaymentController@payStatus')			->name('payStatus');		//Get payment status
Route::post('/gets', 					'HomeController@gets')					->name('gets');

// Route::post('/pay/callback', function(){
// 	return "test";
// });
