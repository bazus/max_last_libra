@extends('layouts.page')
@section('content')

<?php    

    require_once('/var/www/html/librawallet/user_libs/stripe_php/stripe_config.php');

    //var_dump($debug);

    // тестовая сумма в долларах
    //$debug['ammount_usd'] = 750.55;

    $amount_to_pay = $_POST['value'];

    // число платежа для страйпа - в центах
    $amount_to_pay_in_cents = ($_POST['value']) * 100;

?>

<style type="text/css">
/* this is a style-block for card form (later to move to css file)*/
.StripeElement
{
  box-sizing: border-box;
  width: 355px;
  padding: 15px 58px 15px 15px;
  margin: 10px auto;
  border: 1px solid #777;
  border-radius: 7px;
  background-color: white;
  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus
{
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid
{
  border-color: #fa755a;
}

.StripeElement--webkit-autofill
{
  background-color: #fefde5 !important;
}

#stripe_submit_button
{
    width: 355px;
    margin: 0px auto;
    padding: 10px;
    background: white;
    color: white;
    border: 1px solid grey;
    border-radius: 7px;
    font-size: 30px;
    cursor: pointer;
    transition: 0.2s;
    <?php if ($is_stripe_test == 1) {echo 'background: #ccc;';} else {echo 'background: #39298e;';}?>
}

#stripe_submit_button:hover
{
    background: green;
    #color: black;
    transition: 0.2s;
}

</style>

    <section id="payment-visa">
        <div class="container">
            <div class="page-title">Buy Libra Tokens with a Credit Card<br></div>
            <div class="row">
                <!--
                <div class="left col-md-6">
                    <div class="title"></div>
                </div>
                -->
                <div class="right col-md-6" style="margin: 0 auto; text-align: center;">
                    <!--
                    <div class="block-title">Fill in your card info:</div>
                    <div class="block-subtitle">*comission may be applied by Stripe</div>
                    -->
                    <div class="invite-link">
                        <div class="link-label" style="text-align: center; margin-bottom: 10px; font-size: 22pt;">Amount:</div>
                        <div class="link-input" data-toggle="tooltip" data-html="true" data-placement="right" style="margin: 0 auto;">
                            <input type="text" readonly="true" value="{{$amount_to_pay}} USD" name="payment-visa-amount" id="payment-visa-amount" style="border: none; font-size: 20pt; padding: 15px 15px 15px 15px;">
                            <!-- <div class="link-copy"></div> -->
                        </div>
                    </div>
                    <div class="invite-link">
                        <!-- <div class="link-label">Credit or debit card:</div> -->
                            
                            <!-- Start: Stripe card form -->
                            
                            <form action="{{route('stripe_result')}}" method="POST" id="payment_form" style="font-family: Roboto-Regular,sans-serif;">
                                @csrf
                                <div class="stripe_form_row">
                                    <label for="card_element" style="font-size: 14pt;">Enter your bank card details</label>
                                    <div id="card_element">
                                    <!-- A Stripe Element will be inserted here. -->
                                    </div>
                                    <!-- Used to display Element errors. -->
                                    <div id="card_errors" role="alert"></div>
                                    </div>
                                    <br>
                                <button id="stripe_submit_button" type="submit">Submit</button>
                                </form>
                            
                            <!-- End: Stripe card form -->

                    </div>
                </div>
            </div>

    <!-- Start: stripe script -->
    <script type="text/javascript">

    // as it is recomended in the Stripe docs, the Stripe js library v3 is authomaticly connected in the head-tags of evey page on the site

    // создаём клиент Stripe'a and set our pk key
    var stripe = Stripe('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');
    
    // создаём набор элементов
    var elements = stripe.elements();

    // создаём элемент формы
    var card = elements.create('card');

    // вставляем элемент в форму
    card.mount('#card_element');

   
        var form22 = document.getElementById('payment_form');
        var hiddenInput2 = document.createElement('input');
        hiddenInput2.setAttribute('type', '<?php if ($is_stripe_test == 1) {echo "text";} else {echo "hidden";}?>');
        hiddenInput2.setAttribute('name', 'stripe_amount');
        hiddenInput2.setAttribute('value', <?php echo $amount_to_pay_in_cents;?>);
        hiddenInput2.setAttribute('readonly', 'true');
        form22.appendChild(hiddenInput2);
    

    // добавляем слежку за вводом и пишем щамечания
    card.addEventListener('change', function(event)
            {
                var displayError = document.getElementById('card_errors');
                
                if (event.error)
                    {
                        displayError.textContent = event.error.message;
                    }
                else
                    {
                        displayError.textContent = '';
                    }
            }
        );



    // обрабатываем отправку форсы и создаём токен
    var form = document.getElementById('payment_form');
    form.addEventListener('submit', function(event)
        {
            event.preventDefault();

            stripe.createToken(card).then(function(result)
                {
                    if (result.error)
                        {
                            // уведомляем клиента об ошибке
                            var errorElement = document.getElementById('card_errors');
                            errorElement.textContent = result.error.message;
                        }
                    else
                        {
                            // передаём токен в хэндлер
                            stripeTokenHandler(result.token);
                        }

                });

        });

    // вводим токен в форму и отправляем её
    function stripeTokenHandler(token)
        {
            // вставляем скрытое поле с токеном для отправки вместе с основной формой
            var form = document.getElementById('payment_form');

            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // отправляем форму
            form.submit();

        }

    </script>
    <!-- End: stripe script -->

            <div class="debugger"><?php //var_dump($_POST); ?></div>

        </div>

    </section>
             @component('components.footer')
         @endcomponent
    @endsection