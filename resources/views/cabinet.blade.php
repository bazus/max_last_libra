@extends('layouts.page')
@section('content')
<?php
use App\Payment;
$history_class = 'disabled';
$history_class = Payment::SetClass();
?>
    <section id="cabinet">
        <div class="container">
            <div class="user-block wow fadeIn animated">
                <div class="user-logout">
                    <!-- <a href="#">Log out</a> -->
                </div>
                <div class="user-avatar">
                    <img src="assets/images/cabinet/user.png" alt="">
                </div>
                <div class="user-wallet">
                    <div class="wallet-label">Wallet: </div>
                    <div class="wallet-hash">{{$wallet}}</div>
                </div>
                <div class="user-balance">
                    <div class="balance-value">
                        <div class="libra-icon"><img src="assets/images/common/waves-white.png" alt=""></div>{{$coins}}
                    </div>
                    <div class="current-rate">1 Libra = 0,5 $</div>
                    <div class="refresh-balance">Click to refresh</div>
                </div>
                <ul class="total-info wow animated fadeIn" data-wow-delay="1s" data-seconds="{{$time}}">
                    <li>
                        <div class="label">Tokens Purchased:</div>
                        <div class="value" id="tokens-P">6732445</div>
                    </li>
                    <li>
                        <div class="label">Tokens Left:</div>
                        <div class="value" id="tokens-L">13267555</div>
                    </li>
                </ul>
                <ul class="cabinet-menu">
                    <li><a href="{{route('invest')}}"><i class="icon"><img src="assets/images/common/invest.png" alt=""></i>Invest In Libra</a></li>
                    <li><a class="<?php echo $history_class; ?>" href="{{route('history')}}"><i class="icon"><img src="assets/images/common/transactions.png" alt=""></i>Transaction History</a></li>
                </ul>
            </div>
            <ul class="copyrights wow fadeInUp animated">
                <li><img src="assets/images/main/login/facebook.png" alt=""></li>
                <li><img src="assets/images/main/login/libra.svg" alt=""></li>
            </ul>
            <ul class="socials">
                <li><a href="https://t.me/walletlibra" target="_blank" class="telegram wow fadeInLeft animated"></a></li>
                <li><a href="https://twitter.com/libra_wallet" target="_blank" class="twitter wow fadeInRight animated"></a></li>
            </ul>
        </div>
    </section>

    @endsection