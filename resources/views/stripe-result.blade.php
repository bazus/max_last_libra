@extends('layouts.page')
@section('content')

<?php

    use App\Payment;
    

        require_once('/var/www/html/librawallet/user_libs/stripe_php/stripe_config.php');

        // include a stripe library
        require_once('/var/www/html/librawallet/user_libs/stripe_php/init.php');

        $current_user_id = Auth::user()->id;

        // set the secret key
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        // токен создаётся в процессе оплаты и подтверждения формы
        // получаем его из отправленной формы
        $token = $_POST['stripeToken'];

        //получаем сумму, которую спишет страйп - в центах
        $amount_usd_paid_in_cents = ($_POST['stripe_amount']);
        
        // получаем количество долларов
        $amount_usd_paid = $amount_usd_paid_in_cents / 100;

        // создаём оплату
        try
            {
                $charge = \Stripe\Charge::create(
                    [
                        'amount' => $amount_usd_paid_in_cents,
                        'currency' => 'usd',
                        'description' => 'Libra coins purchase',
                        'source' => $token,

                    ]);

                $stripe_succes = true;
            }

        catch (\Stripe\Error\Card $e)

            {
                $date_today = date('d-M-Y-H-i');
                file_put_contents('/var/www/html/librawallet/payment_errors/credit-cards/id-'.$current_user_id.'-payment-error-on-'.$date_today.'.txt', $e);
                $stripe_succes = false;
            }



// if the payment result was successful
if ($stripe_succes === true)
{
    $result_message = 'Purchase has been successfuly created!<br><br>Thank you!';
    Payment::UserPay($current_user_id, $amount_usd_paid);
    Payment::save_to_base_stripe($current_user_id, $amount_usd_paid, 1);
    //var_dump(Auth::user());

}
// if the payment failed
elseif ($stripe_succes === false)
{
    $result_message = 'An error has occured...<br><br>You can try once more';
    Payment::save_to_base_stripe($current_user_id, $amount_usd_paid, 0);
}

?>

<style type="text/css">
    div.go_home
    {
        width: 500px;
        height: 50px;
        margin: 0 auto;
        text-align: center;
    }
    #go_home_button
    {
        width: 100%;
        margin: 0px auto;
        padding: 7px;
        background: white;
        color: black;
        border: 1px solid grey;
        border-radius: 7px;
        font-size: 30px;
        cursor: pointer;
        transition: 0.2s;
    }
    #go_home_button:hover
    {
        background: #39298e;
        color: white;
        transition: 0.2s;
    }
</style>

    <section id="payment-visa">
        <div class="container">
            <div class="page-title"><?php echo $result_message; ?></div>
            <div class="row">
                <div class="left col-md-6">
                    <div class="title">Deposit via Credit Cards<br>Transactions securities are provided by Stripe</div>
                </div>
                <div class="right col-md-6">
                    <div class="block-title">What happens next?</div>
                    <div class="block-subtitle">*comission may be applied by Stripe</div>
                    <div class="invite-link">
                        <div class="link-label">Your transaction is served by Stripe<br>If your bank approves it, you should receive a notice on the subject.<br>Then, when the amount of money you have paid comes to the destination, we will update your Libra wallet.<br></div>
                    </div>
                    <div class="invite-link">
                        <div class="link-label">In case you have got an error, you should receive a special notice.<br>Sometimes it appears that you have insufficient funds or the card is expired.<br>If the problem continues or is surely from our side, please, feel free to contact us and we will help you as best as we can.</div>
                    </div>
                </div>
            </div>
            
            <div class="debugger"><?php //var_dump($_POST); ?></div>

            <div class="go_home"><button id="go_home_button" type="submit" onclick="javascript:document.location.href='https://librawallet.li'">Return back</button></div>
        </div>
    </section>

             @component('components.footer')
         @endcomponent
    @endsection