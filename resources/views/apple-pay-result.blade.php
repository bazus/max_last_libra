@extends('layouts.page')
@section('content')

<?php

    use App\Payment;

    // include a stripe library
    require_once('/var/www/html/librawallet/user_libs/stripe_php/init.php');
    require_once('/var/www/html/librawallet/user_libs/stripe_php/stripe_config.php');

    $current_user_id = Auth::user()->id;

    // set the secret key
    \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
    
    // из формы эппл через ПОСТ пришёл ОБЪЕКТ:
    // получаем токен
    $token = $debug->token;

    //получаем сумму, которую спишет apple - в долларах
    $amount_usd_paid = $debug->amount_to_pay;
    
    // получаем количество центов
    $amount_usd_paid_in_cents = $amount_usd_paid * 100;

    // создаём оплату
    try
        {
            $charge = \Stripe\Charge::create(
                [
                    'amount' => $amount_usd_paid_in_cents,
                    'currency' => 'usd',
                    'description' => 'Libra coins purchase',
                    'source' => $token,

                ]);

            $apple_succes = true;
        }

    catch (\Stripe\Error\Card $e)

        {
            $date_today = date('d-M-Y-H-i');
            file_put_contents('/var/www/html/librawallet/payment_errors/a-g-m-cards/id-'.$current_user_id.'-apple-payment-error-on-'.$date_today.'.txt', $e);
            $apple_succes = false;
        }



    // if the payment result was successful
    if ($apple_succes === true)
    {
        Payment::UserPay($current_user_id, $amount_usd_paid);
        Payment::save_to_base_apple($current_user_id, $amount_usd_paid, 1);
        //var_dump(Auth::user());

    }
    // if the payment failed
    elseif ($apple_succes === false)
    {
        $result_message = 'An error has occured...<br><br>You can try once more';
        Payment::save_to_base_apple($current_user_id, $amount_usd_paid, 0);
    }

?>

@component('components.footer')
@endcomponent
@endsection