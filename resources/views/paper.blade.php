@extends('layouts.page')
@section('content')
    <section id="white-paper">
        <div class="container">
            <div class="page-title">White Paper</div>
            <div class="page-subtitle">Scroll to Read</div>
            <ul class="list">
                <li>
                    <div class="section">
                        <div class="section-number">Section 01</div>
                        <div class="section-title">Introduction</div>
                    </div>
                    <div class="content">
                        <div class="content-title">Libra’s mission is to enable a simple global currency and financial infrastructure that empowers billions of people.</div>
                        <div class="content-text">
                            <p>This document outlines our plans for a new decentralized blockchain, a low-volatility cryptocurrency, and a smart contract platform that together aim to create a new opportunity for responsible financial services innovation.</p>
                        </div>
                        <div class="content-title">Problem Statement</div>
                        <div class="content-text">
                            <p>The advent of the internet and mobile broadband has empowered billions of people globally to have access to the world's knowledge and information, high-fidelity communications, and a wide range of lower-cost, more convenient services. These services are now accessible using a $40 smartphone from almost anywhere in the world.</p>
                            <ol type="1">
                                <li>This connectivity has driven economic empowerment by enabling more people to access the financial ecosystem. Working together, technology companies and financial institutions have also found solutions to help increase economic empowerment around the world. Despite this progress, large swaths of the world's population are still left behind — 1.7 billion adults globally remain outside of the financial system with no access to a traditional bank, even though one billion have a mobile phone and nearly half a billion have internet access.</li>
                                <li>For too many, parts of the financial system look like telecommunication networks pre-internet. Twenty years ago, the average price to send a text message in Europe was 16 cents per message.</li>
                                <li>Now everyone with a smartphone can communicate across the world for free with a basic data plan. Back then, telecommunications prices were high but uniform; whereas today, access to financial services is limited or restricted for those who need it most — those impacted by cost, reliability, and the ability to seamlessly send money.<br> All over the world, people with less money pay more for financial services. Hard-earned income is eroded by fees, from remittances and wire costs to overdraft and ATM charges. Payday loans can charge annualized interest rates of 400 percent or more, and finance charges can be as high as $30 just to borrow $100.</li>
                                <li>When people are asked why they remain on the fringe of the existing financial system, those who remain “unbanked” point to not having sufficient funds, high and unpredictable fees, banks being too far away, and lacking the necessary documentation.</li>
                                <li>Blockchains and cryptocurrencies have a number of unique properties that can potentially address some of the problems of accessibility and trustworthiness. These include distributed governance, which ensures that no single entity controls the network; open access, which allows anybody with an internet connection to participate; and security through cryptography, which protects the integrity of funds.<br> But the existing blockchain systems have yet to reach mainstream adoption. Mass-market usage of existing blockchains and cryptocurrencies has been hindered by their volatility and lack of scalability, which have, so far, made them poor stores of value and mediums of exchange. Some projects have also aimed to disrupt the existing system and bypass regulation as opposed to innovating on compliance and regulatory fronts to improve the effectiveness of anti-money laundering. We believe that collaborating and innovating with the financial sector, including regulators and experts across a variety of industries, is the only way to ensure that a sustainable, secure and trusted framework underpins this new system. And this approach can deliver a giant leap forward toward a lower-cost, more accessible, more connected global financial system.</li>
                            </ol>
                        </div>
                        <div class="content-title">The Opportunity</div>
                        <div class="content-text">
                            <p>As we embark on this journey together, we think it is important to share our beliefs to align the community and ecosystem we intend to spark around this initiative:</p>
                            <ul>
                                <li>We believe that many more people should have access to financial services and to cheap capital.</li>
                                <li>We believe that people have an inherent right to control the fruit of their legal labor.</li>
                                <li>We believe that global, open, instant, and low-cost movement of money will create immense economic opportunity and more commerce across the world.</li>
                                <li>We believe that people will increasingly trust decentralized forms of governance.</li>
                                <li>We believe that a global currency and financial infrastructure should be designed and governed as a public good.</li>
                                <li>We believe that we all have a responsibility to help advance financial inclusion, support ethical actors, and continuously uphold the integrity of the ecosystem.</li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <div class="section-number">Section 02</div>
                        <div class="section-title">Introducing Libra</div>
                    </div>
                    <div class="content">
                     
                        <div class="content-text">
                       <section class="chapter-section" id=""><h4></h4><div><p>The world truly needs a reliable digital currency and infrastructure that together can deliver on the promise of “the internet of money.”</p>
<p>Securing your financial assets on your mobile device should be simple and intuitive. Moving money around globally should be as easy and cost-effective as — and even more safe and secure than — sending a text message or sharing a photo, no matter where you live, what you do, or how much you earn. New product innovation and additional entrants to the ecosystem will enable the lowering of barriers to access and cost of capital for everyone and facilitate frictionless payments for more people.</p>
<p>Now is the time to create a new kind of digital currency built on the foundation of blockchain technology. The mission for <strong>Libra is a simple global currency and financial infrastructure that empowers billions of people.</strong> Libra is made up of three parts that will work together to create a more inclusive financial system:</p>
<ol>
<li>It is built on a secure, scalable, and <strong>reliable blockchain;</strong></li>
<li>It is <strong>backed by a reserve</strong> of assets designed to give it intrinsic value;</li>
<li>It is <strong>governed by the independent Libra Association</strong> tasked with evolving the ecosystem.</li>
</ol>
<p>The Libra currency is built on the “Libra Blockchain.” Because it is intended to address a global audience, the software that implements the Libra Blockchain is open source — designed so that anyone can build on it, and billions of people can depend on it for their financial needs. Imagine an open, interoperable ecosystem of financial services that developers and organizations will build to help people and businesses hold and transfer Libra for everyday use. With the proliferation of smartphones and wireless data, increasingly more people will be online and able to access Libra through these new services. To enable the Libra ecosystem to achieve this vision over time, the blockchain has been built from the ground up to prioritize scalability, security, efficiency in storage and throughput, and future adaptability. Keep reading for an overview of the Libra Blockchain.<p>
<p>The unit of currency is called “Libra.” Libra will need to be accepted in many places and easy to access for those who want to use it. In other words, people need to have confidence that they can use Libra and that its value will remain relatively stable over time. Unlike the majority of cryptocurrencies, Libra is fully backed by a reserve of real assets. A basket of bank deposits and short-term government securities will be held in the Libra Reserve for every Libra that is created, building trust in its intrinsic value. The Libra Reserve will be administered with the objective of preserving the value of Libra over time. Keep reading for an overview of Libra and the reserve.</p>
<p>The Libra Association is an independent, not-for-profit membership organization headquartered in Geneva, Switzerland. The association’s purpose is to coordinate and provide a framework for governance for the network and reserve and lead social impact grant-making in support of financial inclusion. This white paper is a reflection of its mission, vision, and purview. The association’s membership is formed from the network of validator nodes that operate the Libra Blockchain.</p>
<p>Members of the Libra Association will consist of geographically distributed and diverse businesses, nonprofit and multilateral organizations, and academic institutions. The initial group of organizations that will work together on finalizing the association’s charter and become “Founding Members” upon its completion are, by industry:</p>
<ul>
<li>Payments: <strong>Mastercard, Mercado Pago, PayPal, PayU (Naspers’ fintech arm), Stripe, Visa</strong></li>
<li>Technology and marketplaces: <strong>Booking Holdings, eBay, Facebook/Calibra, Farfetch, Lyft</strong><strong>, Spotify AB, Uber Technologies, Inc.</strong></li>
<li>Telecommunications: <strong>Iliad, Vodafone Group</strong></li>
<li>Blockchain: <strong>Anchorage, Bison Trails, Coinbase, Inc., Xapo Holdings Limited</strong></li>
<li>Venture Capital: <strong>Andreessen Horowitz, Breakthrough Initiatives, Ribbit Capital, Thrive Capital, Union </strong><strong>Square Ventures</strong></li>
<li>Nonprofit and multilateral organizations, and academic institutions: <strong>Creative Destruction Lab, Kiva,</strong> <strong>Mercy Corps, Women’s World Banking</strong></li>
</ul>
<p>We hope to have approximately 100 members of the Libra Association by the target launch in the first half of 2020.</p>
<p>Facebook teams played a key role in the creation of the Libra Association and the Libra Blockchain, working with the other Founding Members. While final decision-making authority rests with the association, Facebook is expected to maintain a leadership role through 2019. Facebook created Calibra, a regulated subsidiary, to ensure separation between social and financial data and to build and operate services on its behalf on top of the Libra network.</p>
<p>Once the Libra network launches, Facebook, and its affiliates, will have the same commitments, privileges, and financial obligations as any other Founding Member. As one member among many, Facebook’s role in governance of the association will be equal to that of its peers.</p>
<p>Blockchains are described as either permissioned or permissionless in relation to the ability to participate as a validator node. In a “permissioned blockchain,” access is granted to run a validator node. In a “permissionless blockchain,” anyone who meets the technical requirements can run a validator node. In that sense, Libra will start as a permissioned blockchain.</p>
<p>To ensure that Libra is truly open and always operates in the best interest of its users, our ambition is for the Libra network to become permissionless. The challenge is that as of today we do not believe that there is a proven solution that can deliver the scale, stability, and security needed to support billions of people and transactions across the globe through a permissionless network. One of the association’s directives will be to work with the community to research and implement this transition, which will begin within five years of the public launch of the Libra Blockchain and ecosystem.</p>
<p>Essential to the spirit of Libra, in both its permissioned and permissionless state, the Libra Blockchain will be open to everyone: any consumer, developer, or business can use the Libra network, build products on top of it, and add value through their services. Open access ensures low barriers to entry and innovation and encourages healthy competition that benefits consumers. This is foundational to the goal of building more inclusive financial options for the world.</p></div></section>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <div class="section-number">Section 03</div>
                        <div class="section-title">The Libra Blockchain</div>
                    </div>
                    <div class="content">
                      
                        <div class="content-text">
                            <section class="chapter-section" id=""><h4></h4><div><p>The goal of the Libra Blockchain is to serve as a solid foundation for financial services, including a new global currency, which could meet the daily financial needs of billions of people. Through the process of evaluating existing options, we decided to build a new blockchain based on these three requirements:</p>
<ul>
<li>Able to scale to billions of accounts, which requires high transaction throughput, low latency, and an efficient, high-capacity storage system.</li>
<li>Highly secure, to ensure safety of funds and financial data.</li>
<li>Flexible, so it can power the Libra ecosystem’s governance as well as future innovation in financial services.</li>
</ul>
<p>The Libra Blockchain is designed from the ground up to holistically address these requirements and build on the learnings from existing projects and research — a combination of innovative approaches and well- understood techniques. This next section will highlight <strong>three decisions regarding the Libra Blockchain</strong>:</p>
<ol>
<li>Designing and using the Move programming language.</li>
<li>Using a Byzantine Fault Tolerant (BFT) consensus approach.</li>
<li>Adopting and iterating on widely adopted blockchain data structures.</li>
</ol>
<p><strong>“Move” is a new programming language</strong> for implementing custom transaction logic and “smart contracts” on the Libra Blockchain. Because of Libra’s goal to one day serve billions of people, Move is designed with safety and security as the highest priorities. Move takes insights from security incidents that have happened with smart contracts to date and creates a language that makes it inherently easier to write code that fulfills the author’s intent, thereby lessening the risk of unintended bugs or security incidents. Specifically, Move is designed to prevent assets from being cloned. It enables “resource types” that constrain digital assets to the same properties as physical assets: a resource has a single owner, it can only be spent once, and the creation of new resources is restricted. The Move language also facilitates automatic proofs that transactions satisfy certain properties, such as payment transactions only changing the account balances of the payer and receiver. By prioritizing these features, Move will help keep the Libra Blockchain secure. By making the development of critical transaction code easier, Move enables the secure implementation of the Libra ecosystem’s governance policies, such as the management of the Libra currency and the network of validator nodes. Move will accelerate the evolution of the Libra Blockchain protocol and any financial innovations built on top of it. We anticipate that the ability for developers to create contracts will be opened up over time in order to support the evolution and validation of Move.</p>
<p>To facilitate agreement among all validator nodes on the transactions to be executed and the order in which they are executed, <strong>the Libra Blockchain adopted the BFT approach by using the LibraBFT consensus protocol.</strong> This approach builds trust in the network because BFT consensus protocols are designed to function correctly even if some validator nodes — up to one-third of the network — are compromised or fail. This class of consensus protocols also enables high transaction throughput, low latency, and a more energy-efficient approach to consensus than “proof of work” used in some other blockchains.</p>
<p>In order to securely store transactions, data on the Libra Blockchain is protected by Merkle trees, a data structure used by other blockchains that enables the detection of any changes to existing data. Unlike previous blockchains, which view the blockchain as a collection of blocks of transactions, <strong>the Libra Blockchain is a single data structure that records the history of transactions and states over time.</strong> This implementation simplifies the work of applications accessing the blockchain, allowing them to read any data from any point in time and verify the integrity of that data using a unified framework.</p>
<p><strong>The Libra Blockchain is pseudonymous and allows users to hold one or more addresses that are not linked to their real-world identity.</strong> This approach is familiar to many users, developers, and regulators. The Libra Association will oversee the evolution of the Libra Blockchain protocol and network, and it will continue to evaluate new techniques that enhance privacy in the blockchain while considering concerns of practicality, scalability, and regulatory impact.</p>
<p></p></div></section>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <div class="section-number">Section 04</div>
                        <div class="section-title">The Libra Currency and Reserve</div>
                    </div>
                    <div class="content">
              
                        <div class="content-text">
       <section class="chapter-section" id=""><h4></h4><div><p>We believe that the world needs a global, digitally native currency that brings together the attributes of the world’s best currencies: stability, low inflation, wide global acceptance, and fungibility. The Libra currency is designed to help with these global needs, aiming to expand how money works for more people around the world.</p>
<p><strong>Libra is designed to be a stable digital cryptocurrency that will be fully backed by a reserve of real assets — the Libra Reserve — and supported by a competitive network of exchanges buying and selling Libra.</strong> That means anyone with Libra has a high degree of assurance they can convert their digital currency into local fiat currency based on an exchange rate, just like exchanging one currency for another when traveling. This approach is similar to how other currencies were introduced in the past: to help instill trust in a new currency and gain widespread adoption during its infancy, it was guaranteed that a country’s notes could be traded in for real assets, such as gold. Instead of backing Libra with gold, though, it will be backed by a collection of low-volatility assets, such as bank deposits and short-term government securities in currencies from stable and reputable central banks.</p>
<p>It is important to highlight that this means one Libra will not always be able to convert into the same amount of a given local currency (i.e., Libra is not a “peg” to a single currency). Rather, as the value of the underlying assets moves, the value of one Libra in any local currency may fluctuate. However, the reserve assets are being chosen to minimize volatility, so holders of Libra can trust the currency’s ability to preserve value over time. The assets in the Libra Reserve will be held by a geographically distributed network of custodians with investment-grade credit rating to provide both security and decentralization of the assets.</p>
<p>The assets behind Libra are the major difference between it and many existing cryptocurrencies that lack such intrinsic value and hence have prices that fluctuate significantly based on expectations. Libra is indeed a cryptocurrency, though, and by virtue of that, it inherits several attractive properties of these new digital currencies: the ability to send money quickly, the security of cryptography, and the freedom to easily transmit funds across borders. Just as people can use their phones to message friends anywhere in the world today, with Libra, the same can be done with money — instantly, securely, and at low cost.</p>
<p>Interest on the reserve assets will be used to cover the costs of the system, ensure low transaction fees and support further growth and adoption. The rules for allocating interest on the reserve will be set in advance and will be overseen by the Libra Association. Users of Libra do not receive a return from the reserve.</p>
<p>For more on the reserve policy and the details of the Libra currency.</p></div></section>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <div class="section-number">Section 05</div>
                        <div class="section-title">The Libra Association</div>
                    </div>
                    <div class="content">
              
                        <div class="content-text">
                           <section class="chapter-section" id=""><h4></h4><div><p>To make the mission of Libra a reality — a simple global currency and financial infrastructure that empowers billions of people — the Libra Blockchain and Libra Reserve need a governing entity that is comprised of diverse and independent members. This governing entity is the Libra Association, an independent, not-for-profit membership organization, headquartered in Geneva, Switzerland. Switzerland has a history of global neutrality and openness to blockchain technology, and the association strives to be a neutral, international institution, hence the choice to be registered there. The association is designed to facilitate the operation of the Libra Blockchain; to coordinate the agreement among its stakeholders — the network’s validator nodes — in their pursuit to promote, develop, and expand the network, and to manage the reserve.</p>
<p>The association is governed by the Libra Association Council, which is comprised of one representative per validator node. Together, they make decisions on the governance of the network and reserve. Initially, this group consists of the Founding Members: businesses, nonprofit and multilateral organizations, and academic institutions from around the world. All decisions are brought to the council, and major policy or technical decisions require the consent of two-thirds of the votes, the same supermajority of the network required in the BFT consensus protocol.</p>
<p>Through the association, the validator nodes align on the network’s technical roadmap and development goals. In that sense, it is similar to other not-for-profit entities, often in the form of foundations, which govern open-source projects. As Libra relies on a growing distributed community of open-source contributors to further itself, the association is a necessary vehicle to establish guidance as to which protocols or specifications to develop and to adopt.</p>
<p>The Libra Association also serves as the entity through which the Libra Reserve is managed, and hence the stability and growth of the Libra economy are achieved. The association is the only party able to create (mint) and destroy (burn) Libra. Coins are only minted when authorized resellers have purchased those coins from the association with fiat assets to fully back the new coins. Coins are only burned when the authorized resellers sell Libra coin to the association in exchange for the underlying assets. Since authorized resellers will always be able to sell Libra coins to the reserve at a price equal to the value of the basket, the Libra Reserve acts as a “buyer of last resort.” These activities of the association are governed that can only be changed by a supermajority of the association members.</p>
<p>In these early years of the network, there are additional roles that need to be performed on behalf of the association: the recruitment of Founding Members to serve as validator nodes; the design and implementation of incentive programs to propel the adoption of Libra, including the distribution of such incentives to Founding Members; and the establishment of the association’s social impact grant-making program.</p>
<p>An additional goal of the association is to develop and promote an open identity standard. We believe that decentralized and portable digital identity is a prerequisite to financial inclusion and competition.</p>
<p>An important objective of the Libra Association is to move toward increasing decentralization over time. This decentralization ensures that there are low barriers to entry for both building on and using the network and improves the Libra ecosystem’s resilience over the long term. As discussed above, the association will develop a path toward permissionless governance and consensus on the Libra network. The association’s objective will be to start this transition within five years, and in so doing will gradually reduce the reliance on the Founding Members. In the same spirit, the association aspires to minimize the reliance on itself as the administrator of the Libra Reserve.</p>
<p></p></div></section>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <div class="section-number">Section 06</div>
                        <div class="section-title">What’s Next for Libra?</div>
                    </div>
                    <div class="content">
          
                        <div class="content-text"><section class="chapter-section" id=""><h4></h4><div><p>Today we are publishing this document outlining our goals for Libra and launching libra.org as a home for the association and all things Libra. It will continue to be updated over the coming months. We are also open- sourcing the  code for the Libra Blockchain and launching Libra’s initial testnet for developers to experiment with and build upon.</p>
<p>There is much left to do before the target launch in the first half of 2020.</p>
<p><strong>The Libra Blockchain:</strong></p>
<p>Over the coming months, the association will work with the community to gather feedback on the Libra Blockchain prototype and bring it to a production-ready state. In particular, this work will<br>
focus on ensuring the security, performance, and scalability of the protocol and implementation.</p>
<ul>
<li>The Libra Association will construct well-documented APIs and libraries to enable users to interact with the Libra Blockchain.</li>
<li>The Libra Association will create a framework for the collaborative development of the technology behind the Libra Blockchain using theopen-source methodology. Procedures will be created for discussing and reviewing changes to the protocol and software that support the blockchain.</li>
<li>The association will perform extensive testing of the blockchain, which range from tests of the protocol to constructing a full-scale test of the network in collaboration with entities such as wallet services and exchanges to ensure the system is working before launch.</li>
<li>The association will work to foster the development of the Move language and determine a path for third parties to create smart contracts once language development has stabilized — after the launch of the Libra ecosystem.</li>
</ul>
<p>Together with the community, the association will research the technological challenges on the path to a permissionless ecosystem so that we can meet the objective to begin the transition within five years of the launch.</p>
<p><strong>The Reserve:</strong></p>
<ul>
<li>The association will work to establish a geographically distributed and regulated group of global institutional custodians for the reserve.</li>
<li>The association will establish operational procedures for the reserve to interact with authorized resellers and ensure high-transparency and auditability.</li>
<li>The association will establish policies and procedures that establish how the association can change the composition of the reserve basket.</li>
</ul>
<p><strong>The Libra Association:</strong></p>
<ul>
<li>We will work to grow the Libra Association Council to around 100 geographically distributed and diverse members, all serving as the initial validator nodes of the Libra Blockchain.</li>
<li>The association will develop and adopt a comprehensive charter and set of bylaws for the association on the basis of the currently proposed governance structure.</li>
<li>We will recruit a Managing Director for the association and work with her/him to continue hiring for the association’s executive team.</li>
<li>We will identify social impact partners aligned with our joint mission and will work with them to establish a Social Impact Advisory Board and a social impact program.</li>
</ul></div></section>   
                        </div>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <div class="section-number">Section 07</div>
                        <div class="section-title">How to Get Involved</div>
                    </div>
                    <div class="content">
                        <div class="content-title">                 The association envisions a vibrant ecosystem of developers building apps and services to spur the global use of Libra. The association defines success as enabling any person or business globally to have fair, affordable, and instant access to their money. For example, success will mean that a person working abroad has a fast and simple way to send money to family back home, and a college student can pay their rent as easily as they can buy a coffee.</div>
                        <div class="content-text">
   <section class="chapter-section" id=""><h4></h4><div><p>The association envisions a vibrant ecosystem of developers building apps and services to spur the global use of Libra. The association defines success as enabling any person or business globally to have fair, affordable, and instant access to their money. For example, success will mean that a person working abroad has a fast and simple way to send money to family back home, and a college student can pay their rent as easily as they can buy a coffee.</p>
<p>Our journey is just beginning, and we are asking the community to help. If you believe in what Libra could do for billions of people around the world, share your perspective and join in. Your feedback is needed to make financial inclusion a reality for people everywhere.</p>
<ul>
<li><strong>If you are a researcher or protocol developer,</strong> an early preview of the Libra testnet is available under the Apache 2.0 Open Source License, with accompanying documentation. This is just the start of the process, and the testnet is still an early prototype under development, but you can read, build, and provide feedback right away. Since the current focus is on stabilizing the prototype, the project may initially be slower to take community contributions. However, we are committed to building a community-oriented development process and opening the platform to developers — starting with pull requests — as soon as possible.</li>
<li><strong>If you want to learn about the Libra Association</strong>.</li>
<li><strong>If your organization is interested in becoming a Founding Member or applying for social impact grants from the Libra Association,</strong>.</li>
</ul>
<p>The association will work with the global community in the coming months and continue to partner with policymakers worldwide to further the mission.</p></div></section>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <div class="section-number">Section 08</div>
                        <div class="section-title">Conclusion</div>
                    </div>
                    <div class="content">
                        <div class="content-title"> This is the goal for Libra: A stable currency built on a secure and stable open-source blockchain, backed by a reserve of real assets, and governed by an independent association.</div>
                        <div class="content-text">
                           
Our hope is to create more access to better, cheaper, and open financial services — no matter who you are, where you live, what you do, or how much you have. We recognize that the road to delivering this will be long, arduous, and won’t be achieved in isolation — it will take coming together and forming a real movement around this pursuit. We hope you’ll join us and help turn this dream into a reality for billions of people around the world.



                        </div>
                    </div>
                </li>
            </ul>
            <div class="page-title next">Libra Lexicon</div>
            <ul class="faq-list">
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Blockchain</div>
                        </div>
                        <div class="answer">The technology underpinning cryptocurrency. Blockchain is a technology that can safely store transaction records on a peer-to-peer network instead of storing them in a single location.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Byzantine Fault Tolerant (BFT)</div>
                        </div>
                        <div class="answer">Byzantine Fault Tolerance (BFT) is the ability of a decentralized system to provide safety guarantees in the presence of faulty, or "Byzantine" members. Byzantine Fault Tolerant (BFT) consensus protocols are designed to function correctly even if some validator nodes — up to one-third of the network — are compromised or fail.

</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Consensus protocol</div>
                        </div>
                        <div class="answer">Consensus protocol allows nodes to collectively reach an agreement on whether to accept or reject a transaction.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Cryptocurrency</div>
                        </div>
                        <div class="answer">A digital currency that uses cryptography to verify and secure financial transactions.</div>
                    </div>
                </li>
            </ul>
            <ul class="faq-list next hidden">
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Cryptography</div>
                        </div>
                        <div class="answer">A tool for protecting the integrity of information that is a key component of blockchain technology.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Decentralized</div>
                        </div>
                        <div class="answer">A decentralized network is a network where information is stored across a distributed peer-to-peer network, instead of being stored in a single location.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Digital currency</div>
                        </div>
                        <div class="answer">A type of currency designed to be used in the digital form. A cryptocurrency is a digital currency.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Fiat currency</div>
                        </div>
                        <div class="answer">Fiat currency is an object (like a paper bill or metal coin) that has been established as money, often by a government.</div>
                    </div>
                </li>
            </ul>
            <div class="show-more">Show more</div>
        </div>
    </section>
             @component('components.footer')
         @endcomponent
@endsection