@extends('layouts.page')
@section('content')
    <section id="invest">
        <div class="container">
            <h2 class="page-title wow animated fadeIn"><a href="{{route('wallet')}}" class="back-button"></a><span>Enter Amount</span></h2>
            <form class="wow animated fadeIn" data-wow-delay=".4s" name="user-invest-form" method="POST">
                <div class="form-input">
                    <input id="investValue" type="number" min="50" max="100000" value="500" step="1" name="value">
                    @csrf
                </div>
                <div class="disclaimer animated">min: 50$ max: 100,000$</div>
                <div class="form-info">
                    <div class="coins">
                        <img class="icon" src="assets/images/common/waves.svg" alt="">
                        <div class="value">1000</div>
                        <!-- <div class="label">coins</div> -->
                    </div>
                    <!-- <div class="rate">1$ = 2 Libra coins</div> -->
                </div>
                <div class="form-button">
                    <button type="submit">INVEST IN LIBRA</button>
                    <div class="button-info">Price Rising Soon</div>
                </div>
            </form>
        </div>
    </section>
             @component('components.footer')
         @endcomponent
   @endsection