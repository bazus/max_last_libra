@extends('layouts.page')
@section('content')
    <section id="payment">
        <div class="container">
            <div class="qr wow animated fadeIn">
                <a href="{{route('wallet')}}" class="back-button"></a>
                <img src="{{$qr}}" alt="">
            </div>
            <ul class="order-info">
                <li>
                    <div class="info-label">Amount</div>
                    <div class="info-value">
                        <div class="value">{{$ammount}}</div>
                        <div class="currency">{{$type}}</div>
                    </div>
                </li>
                <li>
                    <div class="info-label">Address</div>
                    <div class="info-value">
                        <div class="address">{{$address}}</div>
                    </div>
                </li>
                <li>
                    <div class="info-label">Time left</div>
                    <div class="info-value">
                        <div class="timer" data-seconds="{{$timeout}}" id="timer">--:--:--</div>
                    </div>
                </li>
            </ul>
            <div class="payment-status">
                <div class="status-label">Status</div>
                <div class="status-value">{{$status_text}}</div>
            </div>
            <div class="payment-alert">Make sure to send enough to cover any coin<br> transaction fees!</div>
            <ul class="faq-list">
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Instructions</div>
                        </div>
                        <div class="answer">Please send the amount indicated above to the specified Address. Make sure to send enough to cover any co in value transaction fees! You will need to initiate the payment using your software or online wallet and copy/paste the address and payment amount into it.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">What if I accidentally don't send enough?</div>
                        </div>
                        <div class="answer">The confirmation process usually takes 10-45 minutes but varies based on the coin's target block time and number of block confirms required.<br> If you don't send enough, that is OK. Just send the remainder and we will combine them for you. You can also send from multiple wallets/accounts.</div>
                    </div>
                </li>
            </ul>

            <div class="debugger"><?php //var_dump($debug); ?></div>

        </div>
    </section>
             @component('components.footer')
         @endcomponent
@endsection