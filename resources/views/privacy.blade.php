@extends('layouts.page')
@section('content')
     <section id="privacy">
        <div class="container-fluid">
            <div class="page-title">Data Policy</div>
            <div class="row">
                <div class="left col-md-3">
                    <div class="links-block wow animated fadeInUp">
                        <div class="list-title">Jump to section</div>
                        <ul class="page-links">
                            <li><a class="scroll-it" href="#data_policy">Libra Association Website Data Policy</a></li>
                            <li><a class="scroll-it" href="#terms_of_use">Libra Association Website Terms of Use</a></li>
                            <li><a class="scroll-it" href="#cookies_policy">Cookies Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="right col-md-9">
                    <ul class="sections">
                        <li id="data_policy">
                            <div class="section-title">Libra Association Website Data Policy</div>
                            <p>This website is owned and operated by the Libra Association (“Libra,” “us,” “our”). This Data Policy applies to the Libra Association website (“Website”). This Data Policy describes our practices for handling your information collected in connection with this Website.</p>
                            <strong>Collection of Information</strong>
                            <p>When you interact with us through our Website, we may collect or receive the following of information:</p>
                            <ul>
                                <li><b>Information you provide directly to us.</b> We may collect information from you such as your name and email address, such as when you subscribe for more information on the Website.</li>
                                <li><b>Information we collect automatically.</b> We may collect certain information automatically when you use our Website, such as your Internet protocol (IP) address, mobile device identifier, browser type, operating system, Internet service provider, pages that you visit before and after using the Website, the date and time of your visit, information about the links you click and pages you view within the Website, and other standard server log information. We may use cookies, pixel tags, local shared objects, and similar technologies to automatically collect this information. By using the Website, you consent to our use of cookies and similar technologies. More information on our use of cookies can be found in our Cookies Policy.</li>
                            </ul>
                            <strong>How We Use Information</strong>
                            <p>Information collected by our Website will help inform the design and implementation of Website. We may also use information you provide us to operate and improve the functionality of the Website.</p>
                            <strong>How We Share Information</strong>
                            <p>We may share your information with certain third parties as set forth below:</p>
                            <ul>
                                <li><b>Authorized third-party vendors and service providers.</b> We share your information with third-party vendors and service providers who support our website, such as by providing technical infrastructure services, business analytics, and data processing.</li>
                                <li><b>Partners.</b> We may share information with the entities that make up the Libra Association (“Partners”). For more information about our Partners, look at the “Association” tab.</li>
                                <li><b>Legal and safety purposes.</b> We may disclose information to respond to subpoenas, court orders, legal process, law enforcement requests, legal claims or government inquiries, detect fraud, and to protect and defend the rights, interests, safety, and security of the website, our affiliates, owner, users, or the public.</li>
                                <li><b>Business transfers.</b> We may share your information in connection with a substantial corporate transaction, such as the sale of a website, a merger, consolidation, asset sale, or in the unlikely event of bankruptcy.</li>
                                <li><b>With your consent.</b> We may share information for any other purposes disclosed to you at the time we collect the information and pursuant to your consent.</li>
                            </ul>
                            <p>If you access third-party services, such as Facebook, Google, or Twitter, through the Website to share information about your experience on the Website with others, these services are outside our control. These third-party services may be able to collect information about you, including information about your activity on the Website, and they may notify your connections on the third-party services about your use of the Website, in accordance with their own privacy policies.</p>
                            <strong>Our Legal Bases For Processing Information</strong>
                            <p>We rely on a variety of legal bases to process data, including:</p>
                            <ul>
                                <li>as necessary to fulfill our Terms;</li>
                                <li>consistent with your consent, which you can revoke at any time;</li>
                                <li>as necessary to comply with our legal obligations;</li>
                                <li>to protect your vital interests, or those of others;</li>
                                <li>as necessary in the public interest;</li>
                                <li>as necessary for our (or others’) legitimate interests, including our interests in providing an innovative personalised, safe and profitable service to our users and partners, unless those interests are overridden by your interests or fundamental rights of freedoms that require protection of personal data.</li>
                            </ul>
                            <p>You may withdraw consent at any time. Such withdrawal of consent will not affect the lawfulness of processing based on consent before its withdrawal.</p>
                            <strong>How You Exercise Your Right</strong>
                            <p>Under applicable laws, you have the right to access, rectify, port, and erase your information, as well as the right to restrict and object to certain processing of your information. You also have the right to object to and restrict certain processing of your data. This includes:</p>
                            <ul>
                                <li>the right to object to our processing of your data for direct marketing, which you can exercise by contacting us</li>
                                <li>the right to object to our processing of your data where we are performing a task in the public interest or pursuing our legitimate interests or those of a third party</li>
                                <li>the right to have us delete your personal information</li>
                            </ul>
                        </li>
                        <li id="terms_of_use">
                            <div class="section-title">Libra Association Website Terms of Use</div>
                            <p>These Terms of Use (“Terms”) govern the use of the Libra Association website and any other website or online service that links to these Terms (collectively, the “Website”). These terms constitute an agreement between you and Libra Association, so it is important that you review them carefully.</p>
                            <strong>Content</strong>
                            <p>The Website is available only for your personal and informational purposes. We make no representations or warranties of any kind as to the accuracy, currency, or completeness of the information and other materials made available through the Website. The Website is not liable for any decisions you may make in reliance of this content.</p>
                            <strong>Your Commitments</strong>
                            <p>We provide these services to you and others. We ask that you make the following commitments:</p>
                            <ul>
                                <li>Not use the Website if you are under 13 years old (or the minimum legal age in your country).</li>
                                <li>You are prohibited from receiving our products, services, or software under applicable laws.</li>
                            </ul>
                            <strong>Prohibited Conduct</strong>
                            <p>You may not access or use, or attempt to access or use, the Website to take any action that could harm us or any third party, interfere with the operation of the Website or use the Website in a manner that violates any laws. For example, and without limitation, you may not:</p>
                            <ul>
                                <li>Share anything that breaches these Terms or other applicable terms;</li>
                                <li>Upload viruses or malicious code or do anything that could disable, overburden, or impair the proper working or appearance of our products;</li>
                                <li>Access or collect data from our products using automated means (without our prior permission) or attempt to access data you do not have permission to access;</li>
                                <li>Engage in any conduct that restricts or inhibits any person from using or enjoying the Website or that, in our sole judgment, exposes us or any of our users, affiliates, or any other third party to any liability, damages, or detriment of any type.</li>
                            </ul>
                            <p>Violations of system or network security may result in liability. We may suspend or terminate your access to the Website for any reason at any time without notice.</p>
                            <strong>Limitation of Liabilities</strong>
                            <p>The website is provided “as is” without warranties of any kind, either express or implied, including without limitation warranties of merchantability, fitness for a particular purpose, title, non-infringement, or other violation of rights. We do not warrant the adequacy, currency, accuracy, likely results, or completeness of the website or any third-party sites linked to or from the website, or that the functions provided will be uninterrupted, virus, or error-free. We expressly disclaim any liability for any errors or omissions in the content included in the website or any third-party sites linked to or from the website. Some jurisdictions may not allow the exclusion of implied warranties, so some of the above exclusions may not apply to you.</p>
                            <p>In no event will we, or our owner, subsidiaries, affiliates, directors, officers, employees, agents, and assigns be liable for any direct or indirect, special, incidental, consequential or punitive damages, lost profits, or other damages whatsoever arising in connection with the use of the website. Any interruption in availability of the website, delay in operation or transmission, computer virus, loss of data, or use, misuse, reliance, review, manipulation, or other utilization in any manner whatsoever of the website or the data collected through the website, even if one or more of them has been advised of the possibility of such damages or loss.</p>
                            <strong>Indemnification</strong>
                            <p>You agree to indemnify, defend, and hold us and our owner, subsidiaries, affiliates, directors, officers, employees, agents and assigns harmless from and against any and all loss, costs, expenses (including reasonable attorneys’ fees and expenses), claims, damages, and liabilities related to or associated with your use of the website and any alleged violation by you of these Terms. We reserve the right to assume the exclusive defense of any claim for which we are entitled to indemnification under this section. In such event, you shall provide us with such cooperation as we reasonably request.</p>
                            <strong>Disputes</strong>
                            <p>If you are a consumer and habitually reside in a Member State of the European Union, the laws of that Member State will apply to any claim, cause of action or dispute that you have against us, which arises out of or relates to these Terms, and you may resolve your claim in any competent court in that Member State that has jurisdiction over the claim. In all other cases, you agree that the claim must be resolved in a competent court in Switzerland and that Swiss law will govern these Terms and any claim, without regard to conflict of law provisions.</p>
                            <strong>Discaimer</strong>
                            <p>LibraWallet offers Complex Financial Products to its clients. Cryptocurrency involves a high level of risk, since the Investment can affect your financial situation both positively and negatively. Using Libra is far from suitable for all investors, as it can result in a complete loss of invested capital. Never invest more than you can afford. Be sure to familiarize yourself with all the risks before you start investing in cryptocurrencies. LibraWallet.li is not in any way affiliated with Facebook, and is a standalone Token created by enthusiasts.</p>
                        </li>
                        <li id="cookies_policy">
                            <div class="section-title">Cookies Policy</div>
                            <strong>Cookies</strong>
                            <p>Our Data Policy explains our principles when it comes to the collection, processing, and storage of your information. This policy specifically explains how we, our partners, and users of our services deploy cookies, as well as the options you have to control them.</p>
                            <strong>What are cookies?</strong>
                            <p>A cookie is a small piece of data, stored in text files, that are stored on your browser or other device when websites are loaded in the browser. Cookies are used to “remember” you and your preferences when you visit the Website either for a single visit (through a “session cookie”) or for multiple repeat visits (called a “persistent cookie”).</p>
                            <p>We use cookies to ensure consistent and efficient experiences for participants in the Website. Cookies also perform functions like allowing participants to remain logged into the Website, if applicable. You can find more information about the types of cookies we use and the purposes for which we use them in the table below:</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td><b>Type of Cookie</b></td>
                                            <td><b>Purpose of Cookie</b></td>
                                        </tr>
                                        <tr>
                                            <td>Strictly Necessary</td>
                                            <td>These cookies are essential in order to enable you to use the Website and its features. The information collected by these cookies relate to the operation of the Website, for example website scripting language and security tokens to maintain secure areas of our Website.</td>
                                        </tr>
                                        <tr>
                                            <td>Performance</td>
                                            <td>These cookies collect anonymous information about how you use our Website, for example which pages you visit most often, whether you receive any error messages, and how you arrived at our Site. Information collected by these cookies is used only to improve your use of Website and never to identify you. These cookies are sometimes placed by third-party providers of web traffic analysis services, such as Google Analytics.</td>
                                        </tr>
                                        <tr>
                                            <td>Analytics</td>
                                            <td>These cookies are placed by trusted third party networks, like Google Analytics, to track details like number of unique visitors and pageviews to improve user experience</td>
                                        </tr>
                                        <tr>
                                            <td>Functionality</td>
                                            <td>These cookies remember choices you make, for example the country you visit our Site from, your language and any changes you have made to text size or other parts of web pages that you can customize, in order to improve your experience of our Website and to make your visits more tailored and enjoyable. The information these cookies collect may be anonymized and cannot be used to track your browsing activity on other websites.</td>
                                        </tr>
                                        <tr>
                                            <td>Third Party / Embedded Content</td>
                                            <td>These cookies enhance the experience of Website users. These cookies allow you to share what you’ve been doing on our Site with social media organizations such as Facebook and Twitter. We have no control over the information collected by these cookies.</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <strong>Which cookies are used and why? </strong>
                                <p>The Libra Association Website uses strictly necessary, performance, analytics, functionality, and embedded content cookies for the purposes of: providing relevant content, analyzing our traffic, and providing a variety of features to you.</p>
                                <strong>How to I disable cookies? </strong>
                                <p>Most internet browsers are automatically set up to accept cookies. However, if you want to refuse or delete any cookies (or similar technologies), please refer to the help and support area on your internet browser for instructions on how to block or delete cookies. Please note you may not be able to take advantage of all the features of our Website, including certain personalised features, if you delete or disable cookies.</p>
                                <strong>Web Beacons</strong>
                                <p>We, or our third party partners, may employ a software technology called web beacons (also known as web bugs, clear gifs or pixels) which helps us understand what content is effective, for example by counting the number of users who have visited these pages, and to understand usage patterns. Web beacons are tiny graphics with a unique identifier, similar in function to cookies, and are used to let us know when content is viewed. In contrast to cookies, which are stored on a user’s computer hard drive, web beacons are embedded on web pages, ads, and e-mail. We, or our third party partners, may tie the information gathered by web beacons to the other information we collect about you.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


     @component('components.footer')
     @endcomponent
@endsection