@extends('layouts.page')
@section('content')
    <section id="main">
        <div class="container">
            <div class="content-block">
                <h1 class="title wow fadeInUp animated" data-wow-delay="0.2s">Welcome to<br> The Libra<br> Wallet</h1>
                <div class="text wow fadeInUp animated" data-wow-delay="0.4s">We are offering a <b>50%</b> Pre-sale discount for Early Investors.<br> You can participate in the birth of Libra and give us the valuable feedback.</div>
            </div>
        </div>
    </section>
    @if(!Auth::user())
    <section id="login">
        <img src="{{ asset('images/main/login/image1.png') }}" alt="" class="image image1 wow  fadeInLeft animated" data-wow-delay="0.2s" data-wow-duration="1.5s">
        <img src="{{ asset('images/main/login/image2.png') }}" alt="" class="image image2 wow  fadeInLeft animated" data-wow-delay="0.4s" data-wow-duration="1.5s">
        <img src="{{ asset('images/main/login/image3.png') }}" alt="" class="image image3 wow  fadeInLeft animated" data-wow-delay="0.6s" data-wow-duration="1.5s">
        <img src="{{ asset('images/main/login/image4.png') }}" alt="" class="image image4 wow  fadeInRight animated" data-wow-delay="0.2s" data-wow-duration="1.5s">
        <img src="{{ asset('images/main/login/image5.png') }}" alt="" class="image image5 wow  fadeInRight animated" data-wow-delay="0.4s" data-wow-duration="1.5s">


        <div class="container">
            <div class="title">Create Your Wallet
            </div>
            <form id="main-login-form" method="POST">
                <div class="form-input">
                    <input type="tel" id="phone" name="login_phone" class="form-control" required placeholder="+12345678999">
                    @csrf
                    <span id="valid-msg" class="hidden">✓ Valid</span>
                    <span id="error-msg" class="hidden"></span>
                </div>
                <div class="form-button">
                    <button id="submit-phone-button" type="submit">Login</button>
                </div>
            </form>
            <ul class="copyrights wow fadeInUp animated">
                <li><img src="{{ asset('images/main/login/facebook.png') }}" alt=""></li>
                <li><img src="{{ asset('images/main/login/libra.svg') }}" alt=""></li>
            </ul>
        </div>
    </section>
    @endif

    <section id="faq">
        <img src="{{ asset('images/main/faq/image1.png') }}" alt="" class="image image1  wow  fadeInLeft animated" data-wow-delay="0.2s" data-wow-duration="1.5s">
        <img src="{{ asset('images/main/faq/image2.png') }}" alt="" class="image image2 wow  fadeInLeft animated" data-wow-delay="0.4s" data-wow-duration="1.5s') }}">
        <img src="{{ asset('images/main/faq/image3.png') }}" alt="" class="image image3 wow  fadeInLeft animated" data-wow-delay="0.6s" data-wow-duration="1.5s') }}">
        <img src="{{ asset('images/main/faq/image4.png') }}" alt="" class="image image4 wow  fadeInRight animated" data-wow-delay="0.2s" data-wow-duration="1.5s') }}">
        <img src="{{ asset('images/main/faq/image5.png') }}" alt="" class="image image5 wow  fadeInRight animated" data-wow-delay="0.4s" data-wow-duration="1.5s') }}">
        <div class="container-fluid">
            <div class="title">Frequently Asked<br> Questions</div>
            <ul class="faq-list">
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Why are you doing a pre-sale?</div>
                        </div>
                        <div class="answer">We have decided to make a private pre-sale specifically for our long-time Facebook users. We also need passionate early adopters and testers for our cryptocurrency to get early feedback from.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">How much will be released for sale?</div>
                        </div>
                        <div class="answer">20 million tokens will be available at 1 Libra token = $ 0.5. After the public release, the price of the raises to 1$ per token.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">When will I be able to use Libra?</div>
                        </div>
                        <div class="answer">The official release is scheduled for 2020, but we want to start Beta testing the exchange sooner. You will be able to pay for goods and services, as well as trade with other Libra Alpha users.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Is Libra a cryptocurrency?</div>
                        </div>
                        <div class="answer">Yes and No. Libra is a new cryptocurrency designed to have a stable and reliable value and be widely accepted around the world.</div>
                    </div>
                </li>
            </ul>
            <ul class="faq-list next hidden">
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">What is the difference between Libra and existing cryptocurrencies?</div>
                        </div>
                        <div class="answer">You will be able to use Libra in the same way you use your local currency today, but Libra isn't violatile and is supported by big companies, such as ours. It is designed to be used by people and businesses around the world.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">What is Libra better than other cryptocurrencies?</div>
                        </div>
                        <div class="answer">Unlike many cryptocurrencies whose values fluctuate based on speculation, Libra is backed by a reserve of assets. This is similar to how other currencies have been introduced in the past — to help instill trust in a new currency and gain widespread adoption, it was guaranteed that a country's notes could be traded in for real assets, like gold. Instead of backing Libra with gold, it will be backed by a collection of established financial assets.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Is Libra safe?</div>
                        </div>
                        <div class="answer">Safety and security are our highest priorities. Security is built into the very design of the Libra Blockchain to prevent malicious activity.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">Is Libra legal?</div>
                        </div>
                        <div class="answer">Libra is legal and can be used in all countries where the use of cryptocurrencies is permitted.</div>
                    </div>
                </li>
            </ul>
            <div class="show-more">Show more</div>
        </div>
    </section>
    <section id="partners">
        <div class="container">
            <div class="row">
                <div class="left col-md-6"><img class="wow  fadeInUp animated" data-wow-delay="0.1s" src="{{ asset('images/main/partners/all.png') }}" alt=""></div>
                <div class="right col-md-6">
                    <h3 class="title wow fadeInUp animated" data-wow-delay="0.3s">Network of partners</h3>
                    <div class="text wow fadeInUp animated" data-wow-delay="0.5s">The Libra Association is governed by diverse businesses, nonprofit and multilateral organizations, and academic institutions. Organizations join the association by running a validator node on the network and serving in governance. An initial group of organizations will work together to finalize the Libra Association's charter and will become the association's Founding Members upon its completion.</div>
                </div>
            </div>
        </div>
    </section>
    <section id="features">
        <div class="container">
            <div class="content-block">
                <h3 class="title wow fadeInUp animated" data-wow-delay="0.1s">Libra is for<br> everyone</h3>
                <div class="text wow fadeInUp animated" data-wow-delay="0.3s">Moving money around the world should be as easy and cheap as sending a text message.No matter where you live, what you do, or how much you earn.</div>
            </div>
            <ul class="features-list">
                <li class="col-md-4">
                    <div class="feature">
                        <div class="feature-icon wow fadeInUp animated" data-wow-delay="0.1s"><img src="{{ asset('images/main/features/icon1.png') }}" alt=""></div>
                        <div class="feature-title wow  fadeInUp animated" data-wow-delay="0.3s">Mobile</div>
                        <div class="feature-text wow  fadeInUp animated" data-wow-delay="0.5s">Libra will be accessible to anyone with an entry-level smartphone and data connectivity.</div>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="feature">
                        <div class="feature-icon wow fadeInUp animated" data-wow-delay="0.1s"><img src="{{ asset('images/main/features/icon2.png') }}" alt=""></div>
                        <div class="feature-title wow  fadeInUp animated" data-wow-delay="0.3s">Stable</div>
                        <div class="feature-text wow  fadeInUp animated" data-wow-delay="0.5s">Libra is backed by a reserve made to keep its value stable.</div>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="feature">
                        <div class="feature-icon wow fadeInUp animated" data-wow-delay="0.1s"><img src="{{ asset('images/main/features/icon3.png') }}" alt=""></div>
                        <div class="feature-title wow  fadeInUp animated" data-wow-delay="0.3s">Fast</div>
                        <div class="feature-text wow  fadeInUp animated" data-wow-delay="0.5s">Libra transactions are quick and easy, no matter where you are sending, or spending, your money.</div>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="feature">
                        <div class="feature-icon wow fadeInUp animated" data-wow-delay="0.1s"><img src="{{ asset('images/main/features/icon4.png') }}" alt=""></div>
                        <div class="feature-title wow  fadeInUp animated" data-wow-delay="0.3s">For the world</div>
                        <div class="feature-text wow  fadeInUp animated" data-wow-delay="0.5s">Libra is a global cryptocurrency that will be available around the world.</div>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="feature">
                        <div class="feature-icon wow fadeInUp animated" data-wow-delay="0.1s"><img src="{{ asset('images/main/features/icon5.png') }}" alt=""></div>
                        <div class="feature-title wow  fadeInUp animated" data-wow-delay="0.3s">Scalable</div>
                        <div class="feature-text wow  fadeInUp animated" data-wow-delay="0.5s">Libra will foster an ecosystem of products and services made to help people use Libra in their everyday lives.</div>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="feature">
                        <div class="feature-icon wow fadeInUp animated" data-wow-delay="0.1s"><img src="{{ asset('images/main/features/icon6.png') }}" alt=""></div>
                        <div class="feature-title wow  fadeInUp animated" data-wow-delay="0.3s">Secure</div>
                        <div class="feature-text wow  fadeInUp animated" data-wow-delay="0.5s">Libra is a cryptocurrency, built on a blockchain designed with security in mind.</div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <section id="paper">
        <div class="container">
            <div class="row">
                <div class="left col-md-6">
                    <img src="{{ asset('images/main/paper/paper.jpg') }}" alt="">
                </div>
                <div class="right col-md-6">
                    <h3 class="title wow animated fadeInUp" data-wow-delay="0.1s">The Libra<br> White Paper</h3>
                    <div class="text wow animated fadeInUp" data-wow-delay="0.3s">The Blockchain. The Currency. The Association.</div>
                    <div class="action wow animated fadeInUp" data-wow-delay="0.5s">
                        <a href="{{route('paper')}}"><span class="action-title">Read the White Paper</span><span class="action-circle"><svg viewBox="0 0 22.03 22.83">
                                    <path d="M10.05 21.41l10.53-9.79L10.05 1.41M19.72 11.66H1"></path>
                                </svg></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
     @component('components.footer')
     @endcomponent
@endsection