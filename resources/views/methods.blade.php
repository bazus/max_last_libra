@extends('layouts.page')
@section('content')
    <section id="methods">
        <div class="container">
            <h2 class="page-title wow animated fadeIn"><a href="{{route('invest')}}" class="back-button"></a><span>Methods</span></h2>
            <ul class="methods-list row">
                <!--
                <li class="col-md-4 wow animated fadeIn" data-wow-delay=".4s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init2('btc')">
                        <span class="method-logo">
                            <img src="assets/images/methods/visa.png" alt="">
                        </span> -->
                       <!--  <span class="method-rate">
                            <span class="value">{{round($ammount, 2)}}</span>
                            <span class="currency">$</span>
                        </span> -->
                   <!-- </a>
                </li>-->

                <li class="col-md-4 wow animated fadeIn" data-wow-delay="1.2s">
                    <a href="#" onclick="event.preventDefault();
                stripe('usd');" class="method">
                        <span class="method-logo">
                            <img src="assets/images/methods/visa.png" style="height: 40px !important;" alt="">
                        </span>
                       <!-- <span class="method-rate">
                            <span class="value">{{round($ammount, 2)}}</span>
                            <span class="currency">$</span>
                        </span> -->
                    </a>
                </li>

                <li class="col-md-4 wow animated fadeIn" data-wow-delay=".4s">
                    <a href="#" class="method" onclick="event.preventDefault();
                apple('usd')">
                        <span class="method-logo">
                            <img src="assets/images/methods/double.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($ammount, 2)}}</span>
                            <span class="currency">$</span>
                        </span> -->
                    </a>
                </li>

                <li class="col-md-4 wow animated fadeIn" data-wow-delay=".6s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init('btc')">
                        <span class="method-logo">
                            <img src="assets/images/methods/bitcoin.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($btc, 6)}}</span>
                            <span class="currency">BTC</span>
                        </span> -->
                    </a>
                </li>

                <li class="col-md-4 wow animated fadeIn" data-wow-delay=".8s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init('ltc')">
                        <span class="method-logo">
                            <img src="assets/images/methods/litecoin.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($ltc, 6)}}</span>
                            <span class="currency">LTC</span>
                        </span> -->
                    </a>
                </li>

                <li class="col-md-4 wow animated fadeIn" data-wow-delay="1s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init('eth')">
                        <span class="method-logo">
                            <img src="assets/images/methods/ethereum.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($eth, 6)}}</span>
                            <span class="currency">ETH</span>
                        </span> -->
                    </a>
                </li>
                
                <li class="col-md-4 wow animated fadeIn" data-wow-delay="1.2s">
                    <a href="#" onclick="event.preventDefault();
                init('bch')" class="method">
                        <span class="method-logo">
                            <img src="assets/images/methods/bitcoin-cash.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($bch, 6)}}</span>
                            <span class="currency">BCH</span>
                        </span> -->
                    </a>
                </li>

                <form id="form" action="{{route('pay')}}" method="post">
                    @csrf
                    <input id="pre_curr" name="value" value="{{$ammount}}" style="display: none;">
                    <input id="curr" name="type" style="display: none;">
                </form>

                <form id="form2" action="{{route('card')}}" method="post">
                    @csrf
                    <input id="pre_curr2" name="value" value="{{$ammount}}" style="display: none;">
                    <input id="curr2" name="type" style="display: none;">
                </form>

                <form id="form3" action="{{route('stripe_card')}}" method="post">
                    @csrf
                    <input id="pre_curr3" name="value" value="{{$ammount}}" style="display: none;">
                    <input id="curr3" name="type" style="display: none;">
                </form>
                
                <form id="form4" action="{{route('apple_pay')}}" method="post">
                    @csrf
                    <input id="pre_curr4" name="value" value="{{$ammount}}" style="display: none;">
                    <input id="curr4" name="type" style="display: none;">
                </form>

            </ul>
        </div>
    </section>

    <script>

    // this code block is responsible for choosing and displaying a payment method on ahref-button click

    var initAcepted = true;

    function init(curr) {
        if(initAcepted){
            initAcepted = false;
            var form = document.getElementById('form');
            document.getElementById('curr').value = curr;
            form.submit();
        }
    }
    function init2(curr) {
        if(initAcepted){
            initAcepted = false;
            var form = document.getElementById('form2');
            document.getElementById('curr2').value = curr;
            form.submit();
        }
    }
    function stripe(curr) {
        if(initAcepted){
            initAcepted = false;
            var form = document.getElementById('form3');
            document.getElementById('curr3').value = curr;
            form.submit();
        }
    }
    function apple(curr) {
        if(initAcepted){
            initAcepted = false;
            var form = document.getElementById('form4');
            document.getElementById('curr4').value = curr;
            form.submit();
        }
    }
    </script>
        
                 @component('components.footer')
         @endcomponent
@endsection