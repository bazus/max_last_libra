@extends('layouts.page')
@section('content')
    <section id="cabinet">
        <div class="container">
            <h2 class="title wow animated fadeIn">Transaction History</h2>
            <div class="transactions-block">
                <ul class="transactions-list">
                    @forelse ( $transactions as $tran)
                  
                        <li class="wow animated fadeInUp" data-wow-delay=".2s">
                        <a href="pay/{{$tran->txn_id}}">    
                            <div class="transaction">
                                <div class="date"><span>
                                <?php
                                    
                                    $var =  DateTime::createFromFormat("Y-m-d H:i:s", $tran->created_at);
                                    
                                    echo $var->format('D, d M Y H:i:s');
                                    ?>

                                
                            </span><span>GMT</span></div>
                                <div class="info">
                                    <div class="type
                                @if($tran->status >= 100)
                                    success
                                @elseif($tran->status < 0)
                                    failure
                                @endif
                                ">Deposit</div>
                                    <div class="value">{{$tran->ammount_usd * 2}} Coins</div>
                                </div>
                            </div>
                        </a>
                    </li>
                    @empty
                        <p>No transactions</p>
                    @endforelse


                    
                </ul>
                <div class="back-button wow animated fadeIn" data-wow-delay="1s">
                    <a href="{{ route('wallet') }}" class="button"><i class="back-icon"></i>Back</a>
                </div>
            </div>
        </div>
    </section>
             @component('components.footer')
         @endcomponent
@endsection