@extends('layouts.page')
@section('content')
    <section id="payment-visa">
        <div class="container">
            <div class="page-title">Buy Libra Tokens with a Credit Card</div>
            <div class="row">
                <div class="left col-md-6">
                    <div class="title">To Deposit via Credit Cards<br> Please use <a class="badge-link" href="https://buy.bitcoin.com/btc/" target="_blank">bitcoin.com</a></div>
                </div>
                <div class="right col-md-6">
                    <div class="block-title">Fill the fields as below:</div>
                    <div class="block-subtitle">≈7.5% +10$ Comission Applied</div>
                    <div class="invite-link">
                        <div class="link-label">Amount</div>
                        <div class="link-input" data-toggle="tooltip" data-html="true" data-placement="right">
                            <input type="text" readonly value="{{round($ammount, 6)}} BTC" name="payment-visa-amount" id="payment-visa-amoun">
                            <div class="link-copy"></div>
                        </div>
                    </div>
                    <div class="invite-link">
                        <div class="link-label">Wallet address</div>
                        <div class="link-input" data-toggle="tooltip" data-html="true" data-placement="right">
                            <input type="text" readonly value="{{$address}}" name="payment-visa-wallet" id="payment-visa-wallet">
                            <div class="link-copy"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-title">Instructions:</div>
            <div class="video-block wow  fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
                <!-- <video src="{{asset('files/learn/learn-infographic-en.mp4')}}" preload="auto" loop="" playsinline="" webkit-playsinline="" x5-playsinline="" style="width: 100%; height: 100%;" autoplay=""></video> -->
                <iframe width="100%" height="100%" style="height: 40vh;" src="https://www.youtube.com/embed/kx0ZdXc8nh4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <ul class="faq-list">
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">How do I Pay?</div>
                        </div>
                        <div class="answer">You should use a bank card on bitcoin.com with your details for the Deposit.<br> The address points to your unique BTC wallet which is used only for refilling your libra wallet.<br> You should go to the partner site and enter the data provided: the number of bitcoins and departure address. Then, just enter your credit card details and the money will be deposited to your Libra Wallet.</div>
                    </div>
                </li>
                <li>
                    <div class="faq-block">
                        <div class="question">
                            <div class="icon plus">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="question-title animated fadeInRight">I paid, what's next?</div>
                        </div>
                        <div class="answer">After payment, wait until your transaction received a required number of confirmations and then money will automatically be credited to your account.<br> On average. it does not take more than 1 hour.</div>
                    </div>
                </li>
            </ul>

            <div class="debugger"><?php //var_dump($debug); ?></div>

        </div>
    </section>
             @component('components.footer')
         @endcomponent
    @endsection