@extends('layouts.page')
@section('content')
<?php 
define("STRIPE_SECRET_KEY", "sk_test_SFbhKwNkuHjHBmsvNhhg5k25");
define("STRIPE_PUBLISHABLE_KEY", "pk_test_ql6Pbd0jnECwm0KBEYy4BJHG");
?>
    <section id="methods">
        <div class="container">
            <h2 class="page-title wow animated fadeIn"><a href="{{route('invest')}}" class="back-button"></a><span>Methods</span></h2>
            <ul class="methods-list row">
                <li class="col-md-4 wow animated fadeIn" data-wow-delay=".4s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init2('btc')">
                        <span class="method-logo">
                            <img src="assets/images/methods/visa.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($ammount, 2)}}</span>
                            <span class="currency">$</span>
                        </span> -->
                    </a>
                </li>
                <li class="col-md-4 wow animated fadeIn" data-wow-delay=".4s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init2('btc')">
                        <span class="method-logo">
                            <img src="assets/images/methods/applepay.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($ammount, 2)}}</span>
                            <span class="currency">$</span>
                        </span> -->
                    </a>
                </li>
                <li class="col-md-4 wow animated fadeIn" data-wow-delay=".6s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init('btc')">
                        <span class="method-logo">
                            <img src="assets/images/methods/bitcoin.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($btc, 6)}}</span>
                            <span class="currency">BTC</span>
                        </span> -->
                    </a>
                </li>
                <li class="col-md-4 wow animated fadeIn" data-wow-delay=".8s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init('ltc')">
                        <span class="method-logo">
                            <img src="assets/images/methods/litecoin.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($ltc, 6)}}</span>
                            <span class="currency">LTC</span>
                        </span> -->
                    </a>
                </li>
                <li class="col-md-4 wow animated fadeIn" data-wow-delay="1s">
                    <a href="#" class="method" onclick="event.preventDefault();
                init('eth')">
                        <span class="method-logo">
                            <img src="assets/images/methods/ethereum.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($eth, 6)}}</span>
                            <span class="currency">ETH</span>
                        </span> -->
                    </a>
                </li>
                
                <li class="col-md-4 wow animated fadeIn" data-wow-delay="1.2s">
                    <a href="#" onclick="event.preventDefault();
                init('bch')" class="method">
                        <span class="method-logo">
                            <img src="assets/images/methods/bitcoin-cash.png" alt="">
                        </span>
                       <!--  <span class="method-rate">
                            <span class="value">{{round($bch, 6)}}</span>
                            <span class="currency">BCH</span>
                        </span> -->
                    </a>
                </li>
               <!--  <li class="col-md-4 wow animated fadeIn" data-wow-delay="1.2s">
                    <a href="#" onclick="event.preventDefault();stripe();
                " class="method">
                        <span class="method-logo">
                            <img src="assets/images/methods/stripe0.png" style="height: 40px !important;" alt="">
                        </span>
                        <span class="method-rate">
                            <span class="value">{{round($ammount, 2)}}</span>
                            <span class="currency">$</span>
                        </span>
                    </a>
                </li> -->
                <form id="form" action="{{route('pay')}}" method="post">
                    @csrf
                    <input name="value" value="{{$ammount}}" style="display: none;">
                    <input id="curr" name="type" style="display: none;">
                </form>

                <form id="form2" action="{{route('card')}}" method="post">
                    @csrf
                    <input name="value" value="{{$ammount}}" style="display: none;">
                    <input id="curr2" name="type" style="display: none;">
                </form>
                <!-- stripe form test-->
                
                 <form id="frmStripePayment" action=""
                method="post">
                <p>Карта для теста: 4242424242424242</p>
              <!--   <div class="field-row">
                    <label>Card Holder Name</label> <span
                        id="card-holder-name-info" class="info"></span><br>
                    <input type="text" id="name" name="name"
                        class="demoInputBox">
                </div> 
                <div class="field-row">
                    <label>Email</label> <span id="email-info"
                        class="info"></span><br> <input type="text"
                        id="email" name="email" class="demoInputBox">
                </div>-->
                <div class="field-row">
                    <label>Card Number</label> <span
                        id="card-number-info" class="info"></span><br> <input
                        type="text" id="card-number" name="card-number"
                        class="demoInputBox">
                </div>
                <div class="field-row">
                    <div class="contact-row column-right">
                        <label>Expiry Month / Year</label> <span
                            id="userEmail-info" class="info"></span><br>
                        <select name="month" id="month"
                            class="demoSelectBox">
                            <option value="08">08</option>
                            <option value="09">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select> <select name="year" id="year"
                            class="demoSelectBox">
                            
                            <option value="19">2019</option>
                            <option value="20">2020</option>
                            <option value="21">2021</option>
                            <option value="22">2022</option>
                            <option value="23">2023</option>
                            <option value="24">2024</option>
                            <option value="25">2025</option>
                            <option value="26">2026</option>
                            <option value="27">2027</option>
                            <option value="28">2028</option>
                            <option value="29">2029</option>
                            <option value="30">2030</option>
                        </select>
                    </div>
                    <div class="contact-row cvv-box">
                        <label>CVC</label> <span id="cvv-info"
                            class="info"></span><br> <input type="text"
                            name="cvc" id="cvc"
                            class="demoInputBox cvv-input">
                    </div>
                </div>
                <div>
                    <input type="submit" name="pay_now" value="Submit"
                        id="submit-btn" class="btnAction"
                        onClick="stripePay(event);">

                    <div id="loader">
                        <img alt="loader" src="LoaderIcon.gif">
                    </div>
                </div>
                <input type='hidden' name='amount' value='{{round($ammount, 2)}}'> <input
                    type='hidden' name='currency_code' value='USD'> <input
                    type='hidden' name='item_name' value='Test Product'>
                <input type='hidden' name='item_number'
                    value='PHPPOTEG#1'>
            </form>
            <style type="text/css">
                #frmStripePayment{
                    display: none;
                }
                #frmStripePayment.show{
                    display: inline-block;
                }
                #frmStripePayment {
    max-width: 300px;
    padding: 25px;
    border: #D0D0D0 1px solid;
    border-radius: 4px;
}

.test-data {
    margin-top: 40px;
}

.tutorial-table {
    border: #D0D0D0 1px solid;
    font-size: 0.8em;
    color: #4e4e4e;
}

.tutorial-table th {
    background: #efefef;
    padding: 12px;
    border-bottom: #e0e0e0 1px solid;
    text-align: left;
}

.tutorial-table td {
    padding: 12px;
    border-bottom: #D0D0D0 1px solid;
}

#frmStripePayment .field-row {
    margin-bottom: 20px;
}

#frmStripePayment div label {
    margin: 5px 0px 0px 5px;
    color: #49615d;
    width: auto;
}

.demoInputBox {
    padding: 10px;
    border: #d0d0d0 1px solid;
    border-radius: 4px;
    background-color: #FFF;
    width: 100%;
    margin-top: 5px;
    box-sizing:border-box;
}

.demoSelectBox {
    padding: 10px;
    border: #d0d0d0 1px solid;
    border-radius: 4px;
    background-color: #FFF;
    margin-top: 5px;
}

select.demoSelectBox {
    height: 35px;
    margin-right: 10px;
}

.error {
    background-color: #FF6600;
    padding: 8px 10px;
    border-radius: 4px;
    font-size: 0.9em;
}

.success {
    background-color: #c3c791;
    padding: 8px 10px;
    border-radius: 4px;
    font-size: 0.9em;
}

.info {
    font-size: .8em;
    color: #FF6600;
    letter-spacing: 2px;
    padding-left: 5px;
}

.btnAction {
    background-color: #586ada;
    padding: 10px 40px;
    color: #FFF;
    border: #5263cc 1px solid;
    border-radius: 4px;
    cursor:pointer;
}

.btnAction:focus {
    outline: none;
}

.column-right {
    margin-right: 6px;
}

.contact-row {
    display: inline-block;
}

.cvv-input {
    width: 60px;
}

#error-message {
    margin: 0px 0px 10px 0px;
    padding: 5px 25px;
    border-radius: 4px;
    line-height: 25px;
    font-size: 0.9em;
    color: #ca3e3e;
    border: #ca3e3e 1px solid;
    display: none;
    width: 300px;
}

#success-message {
    margin: 0px 0px 10px 0px;
    padding: 5px 25px;
    border-radius: 4px;
    line-height: 25px;
    font-size: 0.9em;
    color: #3da55d;
    border: #43b567 1px solid;
    width: 300px;
}

.display-none {
    display:none;
}

#response-container {
    padding: 40px 20px;
    width: 270px;
    text-align:center;
}


.ack-message {
    font-size: 1.5em;
    margin-bottom: 20px;
}

#response-container.success {
    border-top: #b0dad3 2px solid;
    background: #e9fdfa;
}

#response-container.error {
    border-top: #c3b4b4 2px solid;
    background: #f5e3e3;
}

.img-response {
    margin-bottom: 30px;
}

#loader {
    display: none;
}

#loader img {
    width: 45px;
    vertical-align: middle;
}
            </style>
                <!-- end form -->


            </ul>
        </div>
    </section>
    <script >
            var initAcepted = true;
            function init(curr) {
                if(initAcepted){
                    initAcepted = false;
                    var form = document.getElementById('form');
                    document.getElementById('curr').value = curr;
                    form.submit();
                }
            }
            function init2(curr) {
                if(initAcepted){
                    initAcepted = false;
                    var form = document.getElementById('form2');
                    document.getElementById('curr2').value = curr;
                    form.submit();
                }
            }
            function stripe(){
                $('#frmStripePayment').toggleClass('show');
            }
            
        </script>
         <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">
            function cardValidation () {
    var valid = true;
   // var name = $('#name').val();
   // var email = $('#email').val();
    var cardNumber = $('#card-number').val();
    var month = $('#month').val();
    var year = $('#year').val();
    var cvc = $('#cvc').val();

    $("#error-message").html("").hide();

    // if (name.trim() == "") {
    //     valid = false;
    // }
    // if (email.trim() == "") {
    //        valid = false;
    // }
    if (cardNumber.trim() == "") {
           valid = false;
    }

    if (month.trim() == "") {
            valid = false;
    }
    if (year.trim() == "") {
        valid = false;
    }
    if (cvc.trim() == "") {
        valid = false;
    }

    if(valid == false) {
        $("#error-message").html("All Fields are required").show();
    }

    return valid;
}
//set your publishable key
Stripe.setPublishableKey("<?php echo STRIPE_PUBLISHABLE_KEY; ?>");

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        //enable the submit button
        $("#submit-btn").show();
        $( "#loader" ).css("display", "none");
        //display the errors on the form
        $("#error-message").html(response.error.message).show();
    } else {
        //get token id
        var token = response['id'];
        //insert the token into the form
        $("#frmStripePayment").append("<input type='hidden' name='token' value='" + token + "' />");
        //submit form to the server
        // $("#frmStripePayment").submit();
        $('#frmStripePayment').toggleClass('show');
        alert("оплата прошла успешно в тестовом режиме, нужны ключи");
    }
}
function stripePay(e) {
    e.preventDefault();
    var valid = cardValidation();

    if(valid == true) {
        $("#submit-btn").hide();
        $( "#loader" ).css("display", "inline-block");
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#cvc').val(),
            exp_month: $('#month').val(),
            exp_year: $('#year').val()
        }, stripeResponseHandler);

        //submit from callback
        return false;
    }
}
        </script>
                 @component('components.footer')
         @endcomponent
@endsection