@extends('layouts.page')
@section('content')

<?php    

    require_once('/var/www/html/librawallet/user_libs/stripe_php/stripe_config.php');

    //var_dump($debug);

    // тестовая сумма в долларах
    //$debug['ammount_usd'] = 750.55;

    $amount_to_pay = $_POST['value'];

    // число платежа для страйпа - в центах
    $amount_to_pay_in_cents = ($_POST['value']) * 100;

?>

<style type="text/css">
/* this is a style-block for card form (later to move to css file)*/
.StripeElement
{
  box-sizing: border-box;
  width: 355px;
  padding: 15px 58px 15px 15px;
  margin: 10px auto;
  border: 1px solid #777;
  border-radius: 7px;
  background-color: white;
  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus
{
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid
{
  border-color: #fa755a;
}

.StripeElement--webkit-autofill
{
  background-color: #fefde5 !important;
}

#payment-request-button
{
    width: 355px;
    margin: 0px auto;
    padding: 10px;
    background: white;
    color: white;
    border: 1px solid grey;
    border-radius: 7px;
    font-size: 30px;
    cursor: pointer;
    transition: 0.2s;
    <?php if ($is_stripe_test == 1) {echo 'background: #ccc;';} else {echo 'background: #39298e;';}?>
}

#payment-request-button:hover
{
    background: green;
    #color: black;
    transition: 0.2s;
}

div.apple_alert
{
    width: 90%;
    margin: 0 auto;
    padding: 15px 15px 15px 15px;
    border: 1px solid transparent;
    border-radius: 10px;
    background: #39298e;
    color: white;
    font-size: 14pt;
    line-height: 30px;
    cursor: default;
}

div.go_home
{
    width: 500px;
    height: 50px;
    margin: 0 auto;
    text-align: center;
}
#go_home_button
{
    width: 100%;
    margin: 0px auto;
    padding: 7px;
    background: white;
    color: black;
    border: 1px solid grey;
    border-radius: 7px;
    font-size: 30px;
    cursor: pointer;
    transition: 0.2s;
}
#go_home_button:hover
{
    background: #39298e;
    color: white;
    transition: 0.2s;
}

</style>

    <section id="payment-visa">
        <div class="container">
            <div class="page-title">Buy Libra Tokens<br></div>
            <div class="row">
                <!--
                <div class="left col-md-6">
                    <div class="title"></div>
                </div>
                -->
                <div class="right col-md-6" style="margin: 0 auto; text-align: center;">
                    <!--
                    <div class="block-title">Fill in your card info:</div>
                    <div class="block-subtitle">*comission may be applied by Stripe</div>
                    -->
                    <div class="invite-link">
                        <div class="link-label" style="text-align: center; margin-bottom: 10px; font-size: 22pt;">Amount:</div>
                        <div class="link-input" data-toggle="tooltip" data-html="true" data-placement="right" style="margin: 0 auto;">
                            <input type="text" readonly="true" value="{{$amount_to_pay}} USD" name="payment-visa-amount" id="payment-visa-amount" style="border: none; font-size: 20pt; padding: 15px 15px 15px 15px;">
                            <!-- <div class="link-copy"></div> -->
                        </div>
                    </div>
                    <div class="invite-link">
                        <!-- <div class="link-label">Credit or debit card:</div> -->
                            @csrf
                            <!-- Start: Apple Pay element -->
                            
                                    <div id="payment-request-button">
                                        <!-- A Stripe Element will be inserted here. -->
                                    </div>
                            
                            <!-- End: Apple Pay element -->

                    </div>
                </div>
            </div>

            <div class="apple_alert"><p style="font-family: Roboto-Regular,sans-serif;">If you do not see the payment button or it looks inactive or incomplete, then you should check your browser and device properties. Usually, it means and it is highly likeable that you should try another payment method.</p></div>

    <!-- Start: Apple script -->
    <script type="text/javascript">

    // as it is recomended in the Stripe docs, the Stripe js library v3 is authomaticly connected in the head-tags of evey page on the site

    //var allow = htmlIFrameElement.allowPaymentRequest;

    // создаём клиент Stripe'a and set our pk key
    var stripe = Stripe('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');
    
    var paymentRequest = stripe.paymentRequest(
    {
        country: 'US',
        currency: 'usd',
        total: 
            {
                label: 'Libra coins purchase',
                amount: <?php echo $amount_to_pay_in_cents; ?>,
            },
        requestPayerName: true,
        requestPayerEmail: true,
    });

    // создаём набор элементов
    var elements = stripe.elements();

    // создаём элемент
    var prButton = elements.create('paymentRequestButton',
{
paymentRequest: paymentRequest,
style: {
paymentRequestButton:
{
type: 'default', // default: 'default'
theme: 'light', // default: 'dark'
height: '64px', // default: '40px', the width is always '100%'
},
},
});

    // проверяем доступность метода и вставляем элемент в блок
    paymentRequest.canMakePayment().then(function(result)
            {
                if (result)
                    {
                        prButton.mount('#payment-request-button');
                    }
                else
                    {
                        document.getElementById('payment-request-button').style.display = 'none';
                    }
            });

    // достайм долбанный csrf токен для робота
    var csrf = document.getElementsByName('_token')[0].value;
    console.log(csrf);

    paymentRequest.on('token', function(ev)
        {
            fetch("/pay/apple-pay/result", 
                {
                    method: 'POST',
                    body: JSON.stringify(
                        {
                            token: ev.token.id,
                            _token: csrf,
                            amount_to_pay: <?php echo $amount_to_pay; ?>,
                        }),
                    headers: {'content-type': 'application/json'},
                })
            .then(function(response)
            {
                if (response.ok)
                    {
                        alert('Thank you! The payment has been successfuly created. If nothing else prevents it from reaching its destination, you may notice the coins added to your account right now! Please be aware of the fact that your bank may not authorize the transaction.');
                        console.log(response);
                        ev.complete('success');
                        prButton.unmount();
                        getElementById('payment-request-button').remove();
                        querySelector('apple_alert').remove();
                    }
                else
                    {
                        alert('An error has occured. Please, try once more. If the problem keeps occuring, feel free to contact us and we will do everything to help you!');
                        console.log(response);
                        ev.complete('fail');
                    }
            });
        });

    

    </script>
    <!-- End: Apple script -->

            <div class="debugger"><?php //var_dump($_POST); ?></div>
            <br><br>
            <div class="go_home"><button id="go_home_button" type="submit" onclick="javascript:document.location.href='https://librawallet.li'">Return back</button></div>
        </div>

    </section>
             @component('components.footer')
         @endcomponent
    @endsection