@extends('layouts.page')
@section('content')
    <section id="invite">
        <div class="container">
            <h2 class="page-title">Invite Your Friends</h2>
            <div class="subtitle">Earn 9.9% of the amount invested</div>
            <div class="invited-block">
                <div class="invited-counter">
                    <div class="label">Referal:</div>
                    <div class="value">{{$count}}</div>
                </div>
                <div class="earned-info">
                    <div class="info-title">Earned</div>
                    <ul class="info-list">
                        <li>
                            <div class="item-value">{{$ammount}}</div>
                            <div class="item-currency">USD</div>
                        </li>
                        <li>
                            <div class="item-value">{{$count * 100}}</div>
                            <div class="item-currency">coins</div>
                        </li>
                    </ul>
                </div>
                <div class="invite-link">
                    <div class="link-label">Your Link to invite A Friend: </div>
                    <div class="link-input" data-toggle="tooltip" data-html="true" data-placement="right">
                        <input type="text" readonly value="librawallet.li/referal/{{Auth::user()->wallet}}" name="invite-link" id="invite-link">
                        <div class="link-copy"></div>
                    </div>
                </div>
                <div class="withdraw">
                    <div class="withdraw-title">Available to Withdraw</div>
                    <ul class="withdraw-list">
                        <li>
                            <div class="item-value">{{$ammount}}</div>
                            <div class="item-currency">USD</div>
                        </li>
                        <li>
                            <div class="item-value">{{round($btc,5)}}</div>
                            <div class="item-currency">BTC</div>
                        </li>
                    </ul>
                </div>
                <form id="invite-withdraw-form" method="POST">
                    <div class="form-input">
                        <input type="text" name="wallet" placeholder="your BTC wallet">
                        @csrf
                    </div>
                    <div class="form-button">
                        <button type="submit">Withdraw all BTC</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
             @component('components.footer')
         @endcomponent
    @endsection