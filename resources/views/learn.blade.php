@extends('layouts.page')
@section('content')
    <section id="learn-page">
        <div class="head">
            <div class="container">
                <div class="titles-block wow animated fadeInUp">
                    <div class="title">Learn</div>
                    <div class="subtitle">An introduction to the Libra Blockchain.</div>
                </div>
                <div class="links-block wow animated fadeInUp">
                    <div class="list-title">Jump to links</div>
                    <ul class="page-links">
                        <li><a class="scroll-it" href="#faqs">FAQ</a></li>
                        <li><a class="scroll-it" href="#lexicon">Lexicon</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="video-block wow animated fadeIn">
            <div class="container">
                <video src="assets/files/learn/learn-infographic-en.mp4" preload="auto" loop="" playsinline="" webkit-playsinline="" x5-playsinline="" style="width: 100%; height: 100%;" autoplay=""></video>
            </div>
        </div>
        <div class="faqs-block" id="faqs">
            <div class="container">
                <div class="page-title wow animated fadeIn">FAQ</div>
                <ul class="faq-list">
                    <li>
                        <div class="faq-block">
                            <div class="question">
                                <div class="icon plus">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="question-title animated fadeInRight">Why are you doing a pre-sale?</div>
                            </div>
                            <div class="answer">We have decided to make a private pre-sale specifically for our long-time Facebook users. We also need passionate early adopters and testers for our cryptocurrency to get early feedback from.</div>
                        </div>
                    </li>
                    <li>
                        <div class="faq-block">
                            <div class="question">
                                <div class="icon plus">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="question-title animated fadeInRight">How much will be released for sale?</div>
                            </div>
                            <div class="answer">20 million tokens will be available at 1 Libra token = $ 0.5. After the public release, the price of the raises to 1$ per token.</div>
                        </div>
                    </li>
                    <li>
                        <div class="faq-block">
                            <div class="question">
                                <div class="icon plus">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="question-title animated fadeInRight">When will I be able to use Libra?</div>
                            </div>
                            <div class="answer">The official release is scheduled for 2020, but we want to start Beta testing the exchange sooner. You will be able to pay for goods and services, as well as trade with other Libra Alpha users.</div>
                        </div>
                    </li>
                    <li>
                        <div class="faq-block">
                            <div class="question">
                                <div class="icon plus">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="question-title animated fadeInRight">Is Libra a cryptocurrency?</div>
                            </div>
                            <div class="answer">Yes and No. Libra is a new cryptocurrency designed to have a stable and reliable value and be widely accepted around the world.</div>
                        </div>
                    </li>
                </ul>
                <ul class="faq-list next hidden">
                    <li>
                        <div class="faq-block">
                            <div class="question">
                                <div class="icon plus">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="question-title animated fadeInRight">What is the difference between Libra and existing cryptocurrencies?</div>
                            </div>
                            <div class="answer">You will be able to use Libra in the same way you use your local currency today, but Libra isn't violatile and is supported by big companies, such as ours. It is designed to be used by people and businesses around the world.</div>
                        </div>
                    </li>
                    <li>
                        <div class="faq-block">
                            <div class="question">
                                <div class="icon plus">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="question-title animated fadeInRight">What is Libra better than other cryptocurrencies?</div>
                            </div>
                            <div class="answer">Unlike many cryptocurrencies whose values fluctuate based on speculation, Libra is backed by a reserve of assets. This is similar to how other currencies have been introduced in the past — to help instill trust in a new currency and gain widespread adoption, it was guaranteed that a country's notes could be traded in for real assets, like gold. Instead of backing Libra with gold, it will be backed by a collection of established financial assets.</div>
                        </div>
                    </li>
                    <li>
                        <div class="faq-block">
                            <div class="question">
                                <div class="icon plus">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="question-title animated fadeInRight">Is Libra safe?</div>
                            </div>
                            <div class="answer">Safety and security are our highest priorities. Security is built into the very design of the Libra Blockchain to prevent malicious activity.</div>
                        </div>
                    </li>
                    <li>
                        <div class="faq-block">
                            <div class="question">
                                <div class="icon plus">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="question-title animated fadeInRight">Is Libra legal?</div>
                            </div>
                            <div class="answer">Libra is legal and can be used in all countries where the use of cryptocurrencies is permitted.</div>
                        </div>
                    </li>
                </ul>
                <div class="show-more">Show more</div>
            </div>
        </div>
        <div class="lexicon-block" id="lexicon">
            <div class="container">
                <div class="row">
                    <div class="left col-md-6">
                        <div class="title wow fadeInUp" data-wow-delay="0.1s">Libra lexicon</div>
                        <div class="action wow animated fadeInUp" data-wow-delay="0.3s">
                            <a href="{{route('paper')}}"><span class="action-title">Learn the terms</span><span class="action-circle"><svg viewBox="0 0 22.03 22.83">
                                        <path d="M10.05 21.41l10.53-9.79L10.05 1.41M19.72 11.66H1"></path>
                                    </svg></span></a>
                        </div>
                    </div>
                    <div class="right col-md-6">
                        <img src="assets/images/learn/learn-libra-lexicon.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
             @component('components.footer')
         @endcomponent
@endsection