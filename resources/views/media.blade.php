@extends('layouts.page')
@section('content')
    <section id="media">
        <div class="head">
            <div class="container">
                <div class="titles-block wow animated fadeInUp">
                    <div class="title">The Libra media room</div>
                    <div class="subtitle">Download assets and learn about the latest Libra news and initiatives.</div>
                </div>
                <div class="links-block wow animated fadeInUp">
                    <div class="list-title">Jump to links</div>
                    <ul class="page-links">
                        <li><a class="scroll-it" href="#learn">Learn</a></li>
                        <li><a class="scroll-it" href="#download">Download</a></li>
                        <li><a class="scroll-it" href="#read-more">Read More</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section" id="learn">
            <div class="container">
                <div class="section-top">
                    <div class="section-title wow animated fadeIn" data-wow-delay=".2s">Learn</div>
                </div>
                <ul class="learn-slider owl-carousel wow animated fadeIn" data-wow-delay=".4s">
                    <li class="wow animated fadeIn">
                        <a class="block" data-toggle="modal" data-target="#video-modal" data-video-url="oAPkW1B2nZo">
                            <span class="play-button"><svg viewBox="0 0 13.79 14.58">
                                    <path d="M0 0v14.58l13.79-7.29L0 0z"></path>
                                </svg></span>
                            <span class="block-title">The Libra Ecosystem</span>
                            <span class="block-icon"><img src="assets/images/media/learn/icon1.png" alt=""></span>
                        </a>
                    </li>
                    <li class="wow animated fadeIn">
                        <a class="block" data-toggle="modal" data-target="#video-modal" data-video-url="ivOEQErXA5Q">
                            <span class="play-button"><svg viewBox="0 0 13.79 14.58">
                                    <path d="M0 0v14.58l13.79-7.29L0 0z"></path>
                                </svg></span>
                            <span class="block-title">The Libra Association</span>
                            <span class="block-icon"><img src="assets/images/media/learn/icon2.png" alt=""></span>
                        </a>
                    </li>
                    <li class="wow animated fadeIn">
                        <a class="block" data-toggle="modal" data-target="#video-modal" data-video-url="oAPkW1B2nZo">
                            <span class="play-button"><svg viewBox="0 0 13.79 14.58">
                                    <path d="M0 0v14.58l13.79-7.29L0 0z"></path>
                                </svg></span>
                            <span class="block-title">The Libra Ecosystem</span>
                            <span class="block-icon"><img src="assets/images/media/learn/icon1.png" alt=""></span>
                        </a>
                    </li>
                    <li class="wow animated fadeIn">
                        <a class="block" data-toggle="modal" data-target="#video-modal" data-video-url="ivOEQErXA5Q">
                            <span class="play-button"><svg viewBox="0 0 13.79 14.58">
                                    <path d="M0 0v14.58l13.79-7.29L0 0z"></path>
                                </svg></span>
                            <span class="block-title">The Libra Association</span>
                            <span class="block-icon"><img src="assets/images/media/learn/icon2.png" alt=""></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section" id="download">
            <div class="container">
                <div class="section-top">
                    <div class="section-title wow animated fadeIn" data-wow-delay=".2s">Download</div>
                </div>
                <ul class="download-slider owl-carousel wow animated fadeIn" data-wow-delay=".4s">
                    <li>
                        <a href="assets/files/media/WhitePaperAndSupportingDocuments.zip" class="block">
                            <span class="block-icon"></span>
                            <span class="block-title">White Paper and Supporting Documents</span>
                            <span class="block-file">Download PDF Documents</span>
                        </a>
                    </li>
                    <li>
                        <a href="assets/files/media/Libra-Quote-Sheet_English.pdf" class="block">
                            <span class="block-icon"></span>
                            <span class="block-title">Quote Sheet</span>
                            <span class="block-file">Download PDF Document</span>
                        </a>
                    </li>
                    <li>
                        <a href="assets/files/media/Libra-Fact-Sheet_EN.pdf" class="block">
                            <span class="block-icon"></span>
                            <span class="block-title">Fact Sheet</span>
                            <span class="block-file">Download PDF Document</span>
                        </a>
                    </li>
                    <li>
                        <a href="assets/files/media/Libra_Libra-Currency_Static-Infographic_EN.pdf" class="block">
                            <span class="block-icon"></span>
                            <span class="block-title">The Libra Currency</span>
                            <span class="block-file">Download PDF Document</span>
                        </a>
                    </li>
                    <li>
                        <a href="assets/files/media/Libra_Libra-Ecosystem_Static-Infographic_EN.pdf" class="block">
                            <span class="block-icon"></span>
                            <span class="block-title">The Libra Ecosystem</span>
                            <span class="block-file">Download PDF Document</span>
                        </a>
                    </li>
                    <li>
                        <a href="assets/files/media/Libra_Libra-Association_Static-Infographic_EN.pdf" class="block">
                            <span class="block-icon"></span>
                            <span class="block-title">The Libra Association</span>
                            <span class="block-file">Download PDF Document</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section" id="read-more">
            <div class="container">
                <div class="section-top">
                    <div class="section-title wow animated fadeIn" data-wow-delay=".2s">Read More on Libra</div>
                </div>
                <ul class="read-more-slider owl-carousel wow animated fadeIn" data-wow-delay=".4s">
                    <li>
                        <a href="assets/files/media/IntroducingLibra_en_US.pdf" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image1.png" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-quote">Libra News</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image2.png" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-author">Andreessen Horowitz</span>
                                <span class="block-quote">"Mainstreaming crypto can unlock Big Tech and financial innovations for billions."</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image3.jpg" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-author">Ribbit Capital</span>
                                <span class="block-quote">"We believe the world is ready to be inspired and trusted by the promise of Libra."</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image4.png" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-author">Thrive Capital</span>
                                <span class="block-quote">"It’s critical to support ecosystems for transformative technologies like Libra."</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image5.png" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-author">Union Square Ventures</span>
                                <span class="block-quote">"Libra will be an important catalyst in bringing crypto to mainstream users."</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image6.png" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-author">Creative Destruction Lab</span>
                                <span class="block-quote">"We're optimistic Libra will benefit society via new gains from trade."</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image7.png" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-author">Kiva</span>
                                <span class="block-quote">"We're excited to empower people left outside existing financial services."</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image8.png" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-author">Mercy Corps</span>
                                <span class="block-quote">"This could be a financial revolution for the poorest and most vulnerable."</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block">
                            <span class="block-image"><img src="assets/images/media/read-more/image9.png" alt=""></span>
                            <span class="block-bottom">
                                <span class="block-author">Women's World Banking</span>
                                <span class="block-quote">"Women’s World Banking is thrilled to endorse unlocking the potential of Libra!"</span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
             @component('components.footer')
         @endcomponent
@endsection