    <footer>
        <div class="container">
            <div class="left">
                <div class="copyright">© 2019 Libra Association</div>
            </div>
            <div class="right">
                <ul class="links">
                    <li><a href="{{route('privacy'). '#data_policy'}}">Data Policy</a></li>
                    <li><a href="{{route('privacy'). '#cookies_policy'}}">Cookies</a></li>
                    <li><a href="{{route('privacy'). '#terms_of_use'}}">Terms of Use</a></li>
                    <li><a href="{{route('referal')}}">Affiliate program</a></li>
                </ul>
            </div>
        </div>
    </footer>